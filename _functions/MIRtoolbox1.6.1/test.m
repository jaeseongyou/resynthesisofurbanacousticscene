clear all;
clc;
close all;

%% getting the vector to miraudio
x = randn(44100,1); 
x = x/max(abs(x));
fs = 44100;

a = miraudio(x,fs);
f = mirframe(a,2048,'sp',1024,'sp');
fb = mirfilterbank(f);
s = mirspectrum(f,'Window','hamming');
fl = mirflux(s);


audio = mirgetdata(a);
frame = mirgetdata(f);
fbank = mirgetdata(fb);
spec = mirgetdata(s);
flux = mirgetdata(fl);