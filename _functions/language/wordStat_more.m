function [retVal1, retVal2] = wordStat_more(h,N)
    % takes histogram h {uniqueStrings frequencies}
    % takes frequency threshold (return times above N)

    moreThanN = h{1,2}>=N; 
    stringMoreThanN = h{1,1}(find(moreThanN)); 
    freqMoreThanN = h{1,2}(find(moreThanN));
    retVal1 = {stringMoreThanN freqMoreThanN};
    retVal2 = [stringMoreThanN num2cell(freqMoreThanN)];
    
end

