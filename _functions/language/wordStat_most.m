function [retVal1, retVal2] = wordStat_most(h,N)
    % takes histogram h {uniqueStrings frequencies}
    % takes frequency threshold (return N most frequent items)

    [sortedX,sortingIndices] = sort(h{1,2},'descend');
    maxValues = sortedX(1:N);
    maxValueIndices = sortingIndices(1:N);
    maxStrings = h{1,1}(maxValueIndices);
    retVal1 = {maxStrings maxValues};
    retVal2 = [maxStrings num2cell(maxValues)];
    
end

