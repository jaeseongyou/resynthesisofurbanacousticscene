function retVal = fs2ms(fs,sr)
% input structure
%   fs: integer index in sampling rate (vector works, but preferably a single #)
%   sr: sampling rate (e.g. 44.1k)
% output structure
%   an integer value of milisecond)

retVal = round((fs./sr).*1000);
    
end