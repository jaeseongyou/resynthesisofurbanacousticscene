% 2016/05/16
% Jaeseong You
% New York University
% Unit Test for "index2roll_poly.m"

clear all;
clc;
close all;

%% 1) Make test inputs

test_polyGT1 = [1 3 5;
                2 5 6];
test_polyGT2 = [1 2 2;
                2 1 1];
test_polyGT3 = [1 1 1;
                2 1 1];

%% 2) Prepare the correct answers

test_polyGT1_answer = [0 0 1 1 1 0 0;
                       0 0 0 0 1 1 0];
test_polyGT2_answer = [0 1;
                       1 0];                
test_polyGT3_answer = [1;
                       1;
                       0;
                       0];

%% 3) Perform the output test

test_polyGT1_output = index2roll_poly(test_polyGT1,size(test_polyGT1_answer,2),size(test_polyGT1_answer,1));
test_polyGT2_output = index2roll_poly(test_polyGT2,size(test_polyGT2_answer,2),size(test_polyGT2_answer,1));
test_polyGT3_output = index2roll_poly(test_polyGT3,size(test_polyGT3_answer,2),size(test_polyGT3_answer,1));

%% 4) Print the outcome

if isequal(test_polyGT1_answer,test_polyGT1_output)
    display('Passed Poly Test 1');
else
    display('Failed Poly Test 1');
end

if isequal(test_polyGT2_answer,test_polyGT2_output)
    display('Passed Poly Test 2');
else
    display('Failed Poly Test 2');
end

if isequal(test_polyGT3_answer,test_polyGT3_output)
    display('Passed Poly Test 3');
else
    display('Failed Poly Test 3');
end

