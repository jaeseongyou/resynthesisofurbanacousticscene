function retVal = fs2fr(fs,max_fs,max_frame)
% input structure
%   fs: integer index in sampling rate (vector works, but preferably a single #)
%   sr: sampling rate (e.g. 44.1k)
% output structure
%   an integer value of frame index

% compute the retVal
retVal = round((fs./max_fs).*max_frame);

% control for the very beginning
TF = retVal == 0;
retVal(TF) = 1;
 
end