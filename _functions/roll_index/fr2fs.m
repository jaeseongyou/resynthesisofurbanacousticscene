function retVal = fr2fs(fr,max_frame,max_sr)
% input structure
%   fr: integer index of frame (vector works)
%   max_frame: max value of frame index
% output structure
%   an integer value of samping index

% compute the retVal
retVal = round((fr./max_frame).*max_sr);

% control for the very beginning
TF = retVal == 0;
retVal(TF) = 1;
    
end