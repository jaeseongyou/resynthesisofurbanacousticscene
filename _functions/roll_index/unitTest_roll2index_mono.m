% 2016/05/16
% Jaeseong You
% New York University
% Unit Test for "roll2index_mono.m"

clear all;
clc;
close all;

%% 1) Make test inputs

test_binaryGT1 = [1];
test_binaryGT2 = [0 1];
test_binaryGT3 = [0 1 0 1 1 0 0 1];

test_monophonyGT1 = [1 2 3];
test_monophonyGT2 = [1 1 2 3 2 1 1]; 
test_monophonyGT3 = [1 2 2 3 2 2 1]; 


%% 2) Prepare the correct answers

test_binaryGT1_answer = [1 1 1];
test_binaryGT2_answer = [0 1 1;
                         1 2 2];
test_binaryGT3_answer = [0 1 1;
                         1 2 2;
                         0 3 3;
                         1 4 5;
                         0 6 7;
                         1 8 8];
test_monophonyGT1_answer = [1 1 1;
                            2 2 2;
                            3 3 3];
test_monophonyGT2_answer = [1 1 2;
                            2 3 3;
                            3 4 4;
                            2 5 5;
                            1 6 7];
test_monophonyGT3_answer = [1 1 1;
                            2 2 3;
                            3 4 4;
                            2 5 6;
                            1 7 7];

%% 3) Perform the output test

test_binaryGT1_output = roll2index_mono(test_binaryGT1);
test_binaryGT2_output = roll2index_mono(test_binaryGT2);
test_binaryGT3_output = roll2index_mono(test_binaryGT3);
test_monophonyGT1_output = roll2index_mono(test_monophonyGT1);
test_monophonyGT2_output = roll2index_mono(test_monophonyGT2);
test_monophonyGT3_output = roll2index_mono(test_monophonyGT3);

%% 4) Print the outcome

if isequal(test_binaryGT1_answer,test_binaryGT1_output)
    display('Passed Binary Test 1');
else
    display('Failed Binary Test 1');
end

if isequal(test_binaryGT2_answer,test_binaryGT2_output)
    display('Passed Binary Test 2');
else
    display('Failed Binary Test 2');
end

if isequal(test_binaryGT3_answer,test_binaryGT3_output)
    display('Passed Binary Test 3');
else
    display('Failed Binary Test 3');
end

if isequal(test_monophonyGT1_answer,test_monophonyGT1_output)
    display('Passed Monophony Test 1');
else
    display('Failed Monophony Test 1');
end

if isequal(test_monophonyGT2_answer,test_monophonyGT2_output)
    display('Passed Monophony Test 2');
else
    display('Failed Monophony Test 2');
end

if isequal(test_monophonyGT3_answer,test_monophonyGT3_output)
    display('Passed Monophony Test 3');
else
    display('Failed Monophony Test 3');
end