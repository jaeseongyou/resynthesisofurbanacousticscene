function retVal = mono2poly(monoRoll, numClass)
% input 
%   monoRoll: monophonic event roll (number of frames x 1) or transposition
%   numClass: number of classes excluding non-event 0
% output
%   polyphonic event roll (number of class x number of frames)

numFrame = length(monoRoll);
retVal = zeros(numClass,numFrame);

for i = 1:numFrame
    if monoRoll(i)~=0
        retVal(monoRoll(i),i) = 1;
    end
end

end