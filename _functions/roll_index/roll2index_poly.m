function retVal = roll2index_poly(input)
% input structure
%   a table by numClass x maxVal composed of 0s and 1s
% output structure
%   column 1: class index
%   column 2: begin index
%   column 3: end index

retVal = [];

% iterate through each class and interpret it as binary roll2index
numClass = size(input,1);
for i = 1:numClass
    temp = roll2index_binary(input(i,:));
    temp = [ones(size(temp,1),1)*i temp];
    retVal = [retVal;temp];
end

end