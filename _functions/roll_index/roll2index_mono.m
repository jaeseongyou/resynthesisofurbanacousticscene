function retVal = roll2index_mono(input)
% input structure
%   a single row of monophonic ground truth (e.g. 12 12 12 17 17 17 1 1 1...)
%   *Note 1: Row not colunmn
%   *Note 2: Recommended not to use binary ground truth (non-events
%   explicitly stated
% output structure
%   column 1: event class
%   column 2: begin index
%   column 3: end index

%% Finding begin/end indices

% vector control (vertical to horizontal)
if size(input,1)>1;
    input = input';
end

temp = [0 diff(input)];

% get the begin/end indices
begin_index = find(temp~=0);
end_index = begin_index-1;

% manage the very first and last index
begin_index = [1 begin_index];
end_index = [end_index length(input)];

%% Organizing the indices to the output structure

% prepare the final output
retVal = zeros(length(begin_index),3);

% determine the class for each event instance
for i = 1:length(begin_index)
    retVal(i,1) = input(begin_index(i));
end

% fill the second and third columns with the begin and the end indices
retVal(:,2) = begin_index';
retVal(:,3) = end_index';

end