% 2016/05/16
% Jaeseong You
% New York University
% Unit Test for "fs2ms.m"

% clear and close
clear;
clc;
close all;

% global
max_ms = 120300;
max_sr = 44100*120.3;
max_frame = 10334;


%% 1) fs2ms

% 1) generate test input
test_time1 = 1;
test_time2 = 44099;

% 2) Prepare the correct answera
test_time1_answer = 0;
test_time2_answer = 1000;

% 3) Perform the output test

test_time1_output = fs2ms(test_time1,44100);
test_time2_output = fs2ms(test_time2,44100);

% 4) Print the outcome
if isequal(test_time1_answer,test_time1_output)
    display('fs2ms: Passed Time Test 1');
else
    display('fs2ms: Failed Time Test 1 for fs2ms');
end

if isequal(test_time2_answer,test_time2_output)
    display('fs2ms: Passed Time Test 2');
else
    display('fs2ms: Failed Time Test 2');
end


%% 2) ms2fs

% 1) Generate test inputs
test_time1 = 0;
test_time2 = 999.9999;

% 2) Prepare the correct answer
test_time1_answer = 1;
test_time2_answer = 44100;

% 3) Perform the output test
test_time1_output = ms2fs(test_time1,44100);
test_time2_output = ms2fs(test_time2,44100);

% 4) Print the outcome
if isequal(test_time1_answer,test_time1_output)
    display('ms2fs: Passed Time Test 1');
else
    display('ms2fs: Failed Time Test 1');
end

if isequal(test_time2_answer,test_time2_output)
    display('ms2fs: Passed Time Test 2');
else
    display('ms2fs: Failed Time Test 2');
end


%% 3) fs2fr

% 1) Generate test inputs
test_time1 = 1;
test_time2 = max_sr/2;

% 2) Prepare the correct answer
test_time1_answer = 1;
test_time2_answer = max_frame/2;

% 3) Perform the output test
test_time1_output = fs2fr(test_time1,max_sr,max_frame);
test_time2_output = fs2fr(test_time2,max_sr,max_frame);

% 4) Print the outcome
if isequal(test_time1_answer,test_time1_output)
    display('fs2fr: Passed Time Test 1');
else
    display('fs2fr: Failed Time Test 1');
end

if isequal(test_time2_answer,test_time2_output)
    display('fs2fr: Passed Time Test 2');
else
    display('fs2fr: Failed Time Test 2');
end


%% 4) fr2fs

% 1) Generate test inputs
test_time1 = 1;
test_time2 = max_frame/4;

% 2) Prepare the correct answer
test_time1_answer = 1;
test_time2_answer = round(max_sr/4);

% 3) Perform the output test
test_time1_output = fr2fs(test_time1,max_frame,max_sr);
test_time2_output = fr2fs(test_time2,max_frame,max_sr);

% 4) Print the outcome
if isequal(test_time1_answer,test_time1_output)
    display('fr2fs: Passed Time Test 1');
else
    display('fr2fs: Failed Time Test 1');
end

if isequal(test_time2_answer,test_time2_output)
    display('fr2fs: Passed Time Test 2');
else
    display('fr2fs: Failed Time Test 2');
end


%% 5) ms2fr

% 1) Generate test inputs
test_time1 = 1;
test_time2 = max_ms/4;

% 2) Prepare the correct answer
test_time1_answer = 1;
test_time2_answer = round(max_frame/4);

% 3) Perform the output test
test_time1_output = ms2fr(test_time1,max_ms,max_frame);
test_time2_output = ms2fr(test_time2,max_ms,max_frame);

% 4) Print the outcome
if isequal(test_time1_answer,test_time1_output)
    display('ms2fr: Passed Time Test 1');
else
    display('ms2fr: Failed Time Test 1');
end

if isequal(test_time2_answer,test_time2_output)
    display('ms2fr: Passed Time Test 2');
else
    display('ms2fr: Failed Time Test 2');
end



%% 6) fr2ms

% 1) Generate test inputs
test_time1 = 1;
test_time2 = max_frame/4;

% 2) Prepare the correct answer
test_time1_answer = 1;
test_time2_answer = round(max_ms/4);

% 3) Perform the output test
test_time1_output = fr2ms(test_time1,max_frame,max_ms);
test_time2_output = fr2ms(test_time2,max_frame,max_ms);

% 4) Print the outcome
if isequal(test_time1_answer,test_time1_output)
    display('fr2ms: Passed Time Test 1');
else
    display('fr2ms: Failed Time Test 1');
end

if isequal(test_time2_answer,test_time2_output)
    display('fr2ms: Passed Time Test 2');
else
    display('fr2ms: Failed Time Test 2');
end



