function retVal = roll2index_binary(input)
% input structure
%   a single row of binary ground truth (e.g. 0 0 0 1 1 0 1...)
%   *Note 1: Row not colunmn
% output structure
%   column 1: begin index
%   column 2: end index
%% Finding begin/end indices

% vector control (vertical to horizontal)
if size(input,1)>1;
    input = input';
end

temp = [0 diff(input)];

% get the begin/end indices
begin_index = find(temp~=0);
end_index = begin_index-1;

% manage the very first and last index
begin_index = [1 begin_index];
end_index = [end_index length(input)];

%% Organizing the indices to the output structure

% prepare the final output
retVal = zeros(length(begin_index),3);

% determine the class for each event instance
for i = 1:length(begin_index)
    retVal(i,1) = input(begin_index(i));
end

% fill the second and third columns with the begin and the end indices
retVal(:,2) = begin_index';
retVal(:,3) = end_index';

%% Removing 0s and class indices

TF = retVal(:,1)==0;
retVal(TF,:) = [];
retVal(:,1) = [];

end