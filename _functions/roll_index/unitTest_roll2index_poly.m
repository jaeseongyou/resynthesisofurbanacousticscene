% 2016/05/16
% Jaeseong You
% New York University
% Unit Test for "roll2index_poly.m"

clear all;
clc;
close all;

%% 1) Generate test inputs

test_polyGT1 = [0 0 1 1 1 0 0;
                0 0 0 0 1 1 0];
test_polyGT2 = [0 1;
                1 0];                
test_polyGT3 = [1;
                1;
                0;
                0];

%% 2) Prepare the correct answers

test_polyGT1_answer = [1 3 5;
                       2 5 6];
test_polyGT2_answer = [1 2 2;
                       2 1 1];
test_polyGT3_answer = [1 1 1;
                       2 1 1];

%% 3) Perform the output test

test_polyGT1_output = roll2index_poly(test_polyGT1);
test_polyGT2_output = roll2index_poly(test_polyGT2);
test_polyGT3_output = roll2index_poly(test_polyGT3);

%% 4) Print the outcome

if isequal(test_polyGT1_answer,test_polyGT1_output)
    display('Passed Poly Test 1');
else
    display('Failed Poly Test 1');
end

if isequal(test_polyGT2_answer,test_polyGT2_output)
    display('Passed Poly Test 2');
else
    display('Failed Poly Test 2');
end

if isequal(test_polyGT3_answer,test_polyGT3_output)
    display('Passed Poly Test 3');
else
    display('Failed Poly Test 3');
end

