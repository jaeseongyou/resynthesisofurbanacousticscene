function retVal = ms2fr(ms,max_ms,max_frame)
% input structure
%   ms: time in miliseconds (vector works, but preferably a single #)
%   sr: sampling rate (e.g. 44.1k)
% output structure
%   an integer index in sampling rate)

% compute the retVal
retVal = round((ms./max_ms).*max_frame);
    
% control for the very beginning
TF = retVal == 0;
retVal(TF) = 1;
    
end