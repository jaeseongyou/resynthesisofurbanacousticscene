% 2016/05/16
% Jaeseong You
% New York University
% Unit Test for "roll2index_binary.m"

clear all;
clc;
close all;

%% 1) Make test inputs

test_binaryGT1 = [1];
test_binaryGT2 = [0 1];
test_binaryGT3 = [0 1 0 1 1 0 0 1];

%% 2) Prepare the correct answers

test_binaryGT1_answer = [1 1];
test_binaryGT2_answer = [2 2];
test_binaryGT3_answer = [2 2;
                         4 5;
                         8 8];

%% 3) Perform the output test

test_binaryGT1_output = roll2index_binary(test_binaryGT1);
test_binaryGT2_output = roll2index_binary(test_binaryGT2);
test_binaryGT3_output = roll2index_binary(test_binaryGT3);

%% 4) Print the outcome

if isequal(test_binaryGT1_answer,test_binaryGT1_output)
    display('Passed Binary Test 1');
else
    display('Failed Binary Test 1');
end

if isequal(test_binaryGT2_answer,test_binaryGT2_output)
    display('Passed Binary Test 2');
else
    display('Failed Binary Test 2');
end

if isequal(test_binaryGT3_answer,test_binaryGT3_output)
    display('Passed Binary Test 3');
else
    display('Failed Binary Test 3');
end