function retVal = index2roll_binary(input,maxVal)
% input structure
%   input column 1: start time
%   input column 2: end time
%   maxVal : the maximum index of the roll
%   *Note 1: the values of 'input' and 'maxVal' should of a same unit
% output structure 
%   a row vector of 0s and 1s whose length is equal to maxVal

% prepare the retVal
retVal = zeros(1,maxVal);

% iterate through each event and reflect it on the retVal
numEvents = size(input,1);
for i = 1:numEvents
    retVal(input(i,1):input(i,2))=1;
end

end
