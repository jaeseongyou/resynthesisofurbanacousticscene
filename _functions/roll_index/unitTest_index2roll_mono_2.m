% 2016/05/24
% Jaeseong You
% New York University
% Unit Test for "index2roll_mono_2.m"

clear all;
clc;
close all;

%% 1) Make test inputs

test_monophonyGT1 = [1 1 3;
                     2 2 2;
                     3 3 4];

%% 2) Prepare the correct answers

test_monophonyGT1_answer = [1 2 3 3];

%% 3) Perform the output test

test_monophonyGT1_output = index2roll_mono_2(test_monophonyGT1,length(test_monophonyGT1_answer));

%% 4) Print the outcome

if isequal(test_monophonyGT1_answer,test_monophonyGT1_output)
    display('Passed Monophony Test 1');
else
    display('Failed Monophony Test 1');
end
