function retVal = ms2fs(ms,sr)
% input structure
%   ms: time in miliseconds (vector works, but preferably a single #)
%   sr: sampling rate (e.g. 44.1k)
% output structure
%   an integer index in sampling rate)

% compute the retVal
retVal = round((ms./1000).*sr);
    
% control for the very beginning
TF = retVal == 0;
retVal(TF) = 1;
    
end