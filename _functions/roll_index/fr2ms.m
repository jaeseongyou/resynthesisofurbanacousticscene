function retVal = fr2ms(fr,max_frame,max_ms)
% input structure
%   fr: integer index of frame (vector works)
%   max_frame: max value of frame index
% output structure
%   an integer value of miliseconds

retVal = round((fr./max_frame).*max_ms);
    
end