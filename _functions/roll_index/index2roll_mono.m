function retVal = index2roll_mono_2(input,maxVal)
% input structure
%   input column 1  : class index
%   input column 2  : begin index
%   input column 3  : end index
%   maxVal          : the maximum index of the roll
%   *Note 1: the values of 'input' and 'maxVal' should of a same unit
% output structure 
%   a row vector composed of class indices whose length is equal to maxVal

input = sortrows(input,2);

% prepare the retVal
retVal = zeros(1,maxVal);

% iterate through each event and reflect it on the retVal
numEvents = size(input,1);
for i = 1:numEvents
    retVal(input(i,2):input(i,3))=input(i,1);
end

end

