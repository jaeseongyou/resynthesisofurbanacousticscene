function retVal = index2roll_poly(input,maxVal,numClass)
% input structure
%   input column 1  : class index
%   input column 2  : begin index
%   input column 3  : end index
%   maxVal          : the maximum index of the roll
%   numClass        : 16 in this project
%   *Note 1: the values of 'input' and 'maxVal' should of a same unit
% output structure 
%   a table by numClass x maxVal composed of 0s and 1s 

% prepare the retVal
retVal = zeros(numClass,maxVal);

% iternate through each event and reflect it on retVal
numEvent = size(input,1);
for i = 1:numEvent
    retVal(input(i,1),input(i,2):input(i,3)) = 1;
end

end