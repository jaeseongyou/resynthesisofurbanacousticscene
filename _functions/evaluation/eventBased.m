function eventBased(script, fileName, directory, conf)
%%
%MOBILAB 2012 - 2013
%
%   Input parameters
%       -script:        Each row is a representation of a certain class
%                       Each column contains the value (ones) of wich class is active at
%                       that certain moment.
%                       [N x M] matrix:
%                           -N: nr classes
%                           -M: nr of frames in the script
%       -fileName:      Is the name of the output text file.
%       -directory:     Is the location where the output text file must be written
%       -conf:          Is a struct and contains information about the time of the
%                       differrent frames
%%
fileID = fopen([directory filesep fileName],'a');

%Determine the number of classes and the number of framees
[nr_classes nr_frames] = size(script(1:16,:));
%classnames
eventID = {'alert','clearthroat','cough','doorslam','drawer','keyboard','keys',...
            'doorknock','laughter','mouse','pageturn','pendrop','phone','printer','speech','switch'};
k=1;

for class=1:nr_classes
    b = [1 -1];
    changes = conv(script(class,:),b);                                      %Find transitions
    changes = changes(1:length(script));                                    %Apply same lenght as script
    
    onset = find(changes==1);                                               %Find the 0-1 transitions // onset
    offset = find(changes==-1);                                             %Find the 1-0 transitions // offset
    
    for(nr_transtion=1 : length(onset))
        onsetOffset(k,1) = onset(nr_transtion)*(conf.framestep_ms/1000);                            
        onsetOffset(k,2) = offset(nr_transtion)*(conf.framestep_ms/1000);                           
        classifiedEvent{k,1} = eventID{1,class};
        k=k+1;
    end,    
end,

% Sort the data with respect to the onset times
[data_sorted(:,1) I] = sort(onsetOffset(:,1));
data_sorted(:,2) = onsetOffset(I,2);

for (nr=1:length(data_sorted(:,1)))    
fprintf(fileID,'%f',data_sorted(nr,1));                                    %Print onset time
fprintf(fileID, '\t');                                                     %Print space
fprintf(fileID,'%f',data_sorted(nr,2));                                    %Print offset time
fprintf(fileID, '\t');                                                     %Print space
fprintf(fileID,'%s',classifiedEvent{I(nr),1});
fprintf(fileID,'\n');                                                      %Print newline
end,    

fclose(fileID);

end

