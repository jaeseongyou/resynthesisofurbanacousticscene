function [results] = eventDetectionMetrics_classWiseEventBased_binary(eventOutput,eventGT)

% Initialize
eventID = [1]; % 1: event 

% Load event list from output and ground-truth
onset = eventOutput(:,2); offset = eventOutput(:,3); classNames = eventOutput(:,1);
onsetGT = eventGT(:,2); offsetGT = eventGT(:,3); classNamesGT = eventGT(:,1);


% Total number of detected and reference events per class
Ntot = zeros(length(eventID),1);
for i=1:length(onset) % # of detected/annotated events
    pos = classNames(i)==eventID;
    Ntot(pos) = Ntot(pos)+1;
end;

Nref = zeros(length(eventID),1);
for i=1:length(onsetGT)
    pos = classNamesGT(i) == eventID;
    Nref(pos) = Nref(pos)+1;
end;

I = find(Nref>0); % index for classes present in ground-truth


% Number of correctly transcribed events per class, onset within a +/-100 ms range
Ncorr = zeros(length(eventID),1);
NcorrOff = zeros(length(eventID),1);
for j=1:length(onsetGT)
    for i=1:length(onset)
        
        if((classNames(i)==classNamesGT(j)) && (abs(onsetGT(j)-onset(i))<=0.1))
            pos = classNames(i) == eventID;
            Ncorr(pos) = Ncorr(pos)+1;
            
            % If offset within a +/-100 ms range or within 50% of ground-truth event's duration
            if abs(offsetGT(j) - offset(i)) <= max(0.1, 0.5 * (offsetGT(j) - onsetGT(j)))
                pos = classNames(i) == eventID;
                NcorrOff(pos) = NcorrOff(pos)+1;
            end;
            
            break; % In order to not evaluate duplicates
            
        end;
    end;
end;


% Compute onset-only class-wise event-based metrics
Nfp = Ntot-Ncorr;
Nfn = Nref-Ncorr;
Nsubs = min(Nfp,Nfn);
tempRec = Ncorr(I)./(Nref(I)+eps);
tempPre = Ncorr(I)./(Ntot(I)+eps);
results.Rec = mean(tempRec);
results.Pre = mean(tempPre);
tempF =  2*((tempPre.*tempRec)./(tempPre+tempRec+eps));
results.F = mean(tempF);
tempAEER = (Nfn(I)+Nfp(I)+Nsubs(I))./(Nref(I)+eps);
results.AEER = mean(tempAEER);


% Compute onset-offset class-wise event-based metrics
NfpOff = Ntot-NcorrOff;
NfnOff = Nref-NcorrOff;
NsubsOff = min(NfpOff,NfnOff);
tempRecOff = NcorrOff(I)./(Nref(I)+eps);
tempPreOff = NcorrOff(I)./(Ntot(I)+eps);
results.RecOff = mean(tempRecOff);
results.PreOff = mean(tempPreOff);
tempFOff =  2*((tempPreOff.*tempRecOff)./(tempPreOff+tempRecOff+eps));
results.FOff = mean(tempFOff);
tempAEEROff = (NfnOff(I)+NfpOff(I)+NsubsOff(I))./(Nref(I)+eps);
results.AEEROff = mean(tempAEEROff);

end