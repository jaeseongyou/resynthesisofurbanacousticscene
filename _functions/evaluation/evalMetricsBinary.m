function eval = evalMetricsBinary(cMatrix)
% input
%   cMatrix = confusin matrix (Y, prediction)
% output
%   evaluation metrics

eval.tp = cMatrix(2,2);
eval.tn = cMatrix(1,1);
eval.fp = cMatrix(1,2);
eval.fn = cMatrix(2,1);
eval.sens = eval.tp/(eval.tp+eval.fn); % true positive rate
eval.spec = eval.tn/(eval.fp+eval.tn); % true negative rate
eval.prec = eval.tp/(eval.tp+eval.fp); % positive predictive value
eval.npv = eval.tn/(eval.tn+eval.fn); % negative predictive value
eval.acc = (eval.tp+eval.tn)/(eval.tp+eval.fp+eval.tn+eval.fn);
eval.f1 = (2*eval.tp)/(2*eval.tp+eval.fp+eval.fn);

end
