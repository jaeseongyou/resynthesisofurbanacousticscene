% 2016/10/11
% Jaeseong You
% New York University

% Binary ground truth based on all events
%   (1) event-level design matrix 
%   (2) frame-level design matrix

% close and clear
close all; clear; clc;

% 0) add necessary functions to path
addpath('..\_functions\roll_index');
addpath('..\_functions\external');


% 1) path set-up
path2save = 'D:\AED_dev\';
path2save_event = 'D:\AED_dev\gt_archive\binary\allEvent\eventLevel_X\';
path2save_frame = 'D:\AED_dev\gt_archive\binary\allEvent\frameLevel_X\';
path2feature = 'D:\AED_dev\acousticScene_feature\';
dir2feature = dir(path2feature);
numFile = length(dir2feature)-2;

display('step 1 completed');


% 2) variables set-up
num_feature = 81; % check in the feature-rendering
fs = 44100;
max_ms = 120300;
max_fr = 2582; % featue matrix is (4096 x 2582)
time = (1:max_ms)/1000;

display('step 2 completed');


% 3) prepare the annotation data
% read in raw annotation data & rearrange
fid = fopen('D:\AED_dev\rawData_archive\events_rendered.csv');
raw = textscan(fid,'%s','delimiter',',');
fclose(fid);

% rearrange & remove 'scene's
arranged = reshape(raw{1,1},13,[])';
arranged(strcmp(arranged(:,13),'scene'),:)=[];

% figure out the # of annotators per segment
segID_annID = sortrows(str2double(arranged(:,3:4)),1);
annID = segID_annID(:,2);
segmentTurn = find(diff(segID_annID(:,1))>=1);
uniqueSegID = unique(segID_annID(:,1));
segID_annNum = [uniqueSegID zeros(length(uniqueSegID),1)];
for i = 1:length(uniqueSegID) % iterate through segments
    begIdx = [1;(segmentTurn+1)]; endIdx = [segmentTurn;length(annID)];
    segID_annNum(i,2) = length(unique(annID(begIdx(i):endIdx(i))));
end

% pick only the ones that are annotated by 5 or more
segID_annNum(segID_annNum(:,2)<=4,:) = [];
segID = segID_annNum(:,1);

% get the final "event" table (sort, str2 double, onset/offset control)
event = str2double(arranged(:,3:6));
event = sortrows(event,2);
event = sortrows(event,1);
event(event(:,3)<1) = 1; % onset control
event(event(:,4)>max_ms) = max_ms; % offset control

display('step 3 completed');


% 4) all-event GT computation
for i = 1:length(segID) % iterate through each segement
    
    % load audio feature
    load (strcat(path2feature,'featurePerSeg.',num2str(segID(i)),'.mat'));
    
    % get annotations of corresponding segment
    eventPerSeg = event(event(:,1)==segID(i),:);
    annIdPerSeg = unique(eventPerSeg(:,2));
    annNumPerSeg = length(annIdPerSeg);
    
    % getting the roll-form GT per seg
    roll_ms = zeros(annNumPerSeg,max_ms);
    roll_fr = zeros(annNumPerSeg,max_fr);
    for j = 1:annNumPerSeg %
        eventPerAnn_ms = eventPerSeg(eventPerSeg(:,2)==annIdPerSeg(j),3:4);
        offset_fr = ms2fr(eventPerAnn_ms(:,2),max_ms,max_fr); % check if there is error in allEvent
        onset_fr = [1;offset_fr(1:end-1)+1];
        eventperAnn_fr = [onset_fr offset_fr];
        roll_ms(j,:) = index2roll_binary(eventPerAnn_ms,max_ms);
        roll_fr(j,:) = index2roll_binary(ms2fr(eventPerAnn_ms,max_ms,max_fr),max_fr); 
    end
    
    % frame_level design X & save
    binaryGT_allEvent_frameLevel_X_seg = [featurePerSeg roll_fr'];
    filename = strcat(path2save_frame,'binaryGT_allEvent_frameLevel_X_',num2str(segID(i)),'.mat');
    save(filename,'binaryGT_allEvent_frameLevel_X_seg');
        
    % event_level design X
    event_fr = [];
    for j = 1:annNumPerSeg
        event_fr = [event_fr;roll2index_mono(roll_fr(j,:))];
    end
    featureSumPerEvent = zeros(size(event_fr,1),num_feature);
    for j = 1:size(event_fr,1)
        featureSumPerEvent(j,:) = mean(featurePerSeg(event_fr(j,2):event_fr(j,3),:),1);
    end
    binaryGT_allEvent_eventLevel_X_seg = [featureSumPerEvent event_fr(:,1)];
    filename = strcat(path2save_event,'binaryGT_allEvent_eventLevel_X_',num2str(segID(i)),'.mat');
    save(filename,'binaryGT_allEvent_eventLevel_X_seg');
    
end

display('step 4 completed');

