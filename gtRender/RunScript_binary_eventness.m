% 2016/10/13
% Jaeseong You
% New York University

% Binary ground truth based on eventness score
%   (1) event-level design matrix
%   (2) frame-level design matrix
%   (3) visualization

% close and clear
close all; clear; clc;

% 0) add necessary functions to path
addpath('..\_functions\roll_index');
addpath('..\_functions\external');


% 1) path set-up
path2majority = 'D:\AED_dev\gt_archive\binary\majority\frameLevel_X\'; % visualization purpose
path2save = 'D:\AED_dev\';
path2save_event = 'D:\AED_dev\gt_archive\binary\eventness\eventLevel_X\';
path2save_frame = 'D:\AED_dev\gt_archive\binary\eventness\frameLevel_X\';
path2save_fig = 'D:\AED_dev\gt_archive\binary\eventness\figure\';
path2feature = 'D:\AED_dev\acousticScene_feature\';
dir2feature = dir(path2feature);
numFile = length(dir2feature)-2;

display('step 1 completed');


% 2) variables set-up
num_feature = 81; % check in the feature-rendering
fs = 44100;
max_ms = 120300;
max_fr = 2582; % featue matrix is (4096 x 2582)
time = (1:max_fr)/max_fr*120.3;
time_ms =(1:max_ms)/1000;

display('step 2 completed');


% 3) prepare the annotation data

% read in raw annotation data & rearrange
fid = fopen('D:\AED_dev\rawData_archive\events_rendered.csv');
raw = textscan(fid,'%s','delimiter',',');
fclose(fid);

% rearrange & remove 'scene's
arranged = reshape(raw{1,1},13,[])';
arranged(strcmp(arranged(:,13),'scene'),:)=[];

% figure out the # of annotators per segment
segID_annID = sortrows(str2double(arranged(:,3:4)),1);
annID = segID_annID(:,2);
segmentTurn = find(diff(segID_annID(:,1))>=1);
uniqueSegID = unique(segID_annID(:,1));
segID_annNum = [uniqueSegID zeros(length(uniqueSegID),1)];
for i = 1:length(uniqueSegID) % iterate through segments
    begIdx = [1;(segmentTurn+1)]; endIdx = [segmentTurn;length(annID)];
    segID_annNum(i,2) = length(unique(annID(begIdx(i):endIdx(i))));
end

% pick only the ones that are annotated by 5 or more
segID_annNum(segID_annNum(:,2)<=4,:) = [];
segID = segID_annNum(:,1);

% get the final "event" table (str2 double, sort, onset/offset control)
event = str2double(arranged(:,[3 4 5 6 9 10 11]));
event = sortrows(event,2);
event = sortrows(event,1);
event(event(:,3)<1) = 1; % onset control
event(event(:,4)>max_ms) = max_ms; % offset control

display('step 3 completed');


% 4) eventness GT computation
for i = 1:length(segID) % iterate through each segement
    
    % load audio feature
    load (strcat(path2feature,'featurePerSeg.',num2str(segID(i)),'.mat'));
    
    % get annotations of corresponding segment
    eventPerSeg = event(event(:,1)==segID(i),:);
    annIdPerSeg = unique(eventPerSeg(:,2));
    annNumPerSeg = length(annIdPerSeg);
    
    % eventness score
    score_ms = zeros(1,max_ms);
    for j = 1:size(eventPerSeg)
        arousal = eventPerSeg(j,5);
        presence = eventPerSeg(j,7);
        score_ms(eventPerSeg(j,3):eventPerSeg(j,4)) = score_ms(eventPerSeg(j,3):eventPerSeg(j,4))+presence+arousal/2;
    end

    % score-thresholding
    scoreThr_ms = score_ms > mean(score_ms); 
    %scoreThr_ms = score_ms > movmean(score_ms,101); % not working due to the lack of continuity
    scoreThr_ms = scoreThr_ms + 0; % logic to integer
    
    % median pass
    scoreThr_ms = medfilt1(scoreThr_ms,101); % 101 ms
    eventForm_ms = roll2index_mono(scoreThr_ms);
    offset_fr = ms2fr(eventForm_ms(:,3),max_ms,max_fr);
    onset_fr = [1;(offset_fr(1:end-1)+1)];
    eventForm_fr = [eventForm_ms(:,1) onset_fr offset_fr];
    
    % event_level design X
    featureSumPerEvent = zeros(size(eventForm_fr,1),num_feature);
    for j = 1:size(eventForm_fr,1)
        featureSumPerEvent(j,:) = mean(featurePerSeg(eventForm_fr(j,2):eventForm_fr(j,3),:),1);
    end
    binaryGT_eventness_eventLevel_X_seg = [featureSumPerEvent eventForm_fr(:,1)];
    filename = strcat(path2save_event,'binaryGT_eventness_eventLevel_X_',num2str(segID(i)),'.mat');
    save(filename,'binaryGT_eventness_eventLevel_X_seg');    
    
    % frame_level design X & save
    rollForm_fr = index2roll_mono(eventForm_fr,max_fr);
    binaryGT_eventness_frameLevel_X_seg = [featurePerSeg rollForm_fr'];
    filename = strcat(path2save_frame,'binaryGT_eventness_frameLevel_X_',num2str(segID(i)),'.mat');
    save(filename,'binaryGT_eventness_frameLevel_X_seg');
    
    % visualize (majority GT, outcome, continuous with threhold)  
    h = figure(1);
    
    g1 = subplot(3,1,1);
    plot(time_ms,score_ms); hold on; % continuous eventness
    threshold = ones(max_ms,1)*mean(score_ms);
    plot(time_ms,threshold);hold off;
    set(gca,'YTick',max(score_ms)/2,'YTickLabel',{'cont.'},'TickLength',[0 0]);
    title(strcat('seg.',num2str(segID(i))));
    p = get(g1,'position'); p(4) = p(4)*.4; set(g1,'position',p); % height control    
    xlim([0 120.3]);
    
    g2 = subplot(3,1,2);
    imagesc(time,1,rollForm_fr); % eventness
    colormap(flipud(gray));
    set(gca,'YTick',1,'YTickLabel',{'disc.'},'TickLength',[0 0]);
    p = get(g2,'position'); p(4) = p(4)*.4; set(g2,'position',p); % height control    
    
    g3 = subplot(3,1,3);
    load(strcat(path2majority,'binaryGT_majority_frameLevel_X_',num2str(segID(i))));
    imagesc(time,1,binaryGT_majority_frameLevel_X_seg(:,end)'); % majority
    colormap(flipud(gray));
    set(gca,'YTick',1,'YTickLabel',{'majority'},'TickLength',[0 0]);
    p = get(g3,'position'); p(4) = p(4)*.4; set(g3,'position',p); % height control     
        
    % save
    filename = strcat('binaryGT_eventness_',num2str(segID(i)),'.fig');
    set(h,'Position',[300 100 500 100*(annNumPerSeg+1)]);
    saveas(gcf,strcat(path2save_fig,filename));
    
    % figure close
    close all;
end

display('step 4 completed');

