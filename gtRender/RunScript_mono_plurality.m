% 2016/10/15
% Jaeseong You
% New York University

% Monophonic ground truth based on plurality
%   (1) event-level design matrix 
%   (2) frame-level design matrix
%   (3) visualization

% close and clear
close all; clear; clc;

% 0) add necessary functions to path
addpath('..\_functions\roll_index');
addpath('..\_functions\external');


% 1) path set-up
path2save = 'D:\AED_dev\';
path2save_event = 'D:\AED_dev\gt_archive\mono\plurality\eventLevel_X\';
path2save_frame = 'D:\AED_dev\gt_archive\mono\plurality\frameLevel_X\';
path2save_fig = 'D:\AED_dev\gt_archive\mono\plurality\figure\';
path2feature = 'D:\AED_dev\acousticScene_feature\';
dir2feature = dir(path2feature);
numFile = length(dir2feature)-2;

display('step 1 completed');


% 2) variables set-up
num_feature = 81; % check in the feature-rendering
fs = 44100;
max_ms = 120300;
max_fr = 2582; % featue matrix is (4096 x 2582)
time = (1:max_ms)/1000;

display('step 2 completed');


% 3) prepare the annotation data
% read in raw annotation data
fid = fopen('D:\AED_dev\rawData_archive\events_rendered.csv');
raw = textscan(fid,'%s','delimiter',',');
fclose(fid);

% rearrange & remove 'scene's
arranged = reshape(raw{1,1},13,[])';
arranged(strcmp(arranged(:,13),'scene'),:)=[];

% figure out the # of annotators per segment
segID_annID = sortrows(str2double(arranged(:,3:4)),1);
annID = segID_annID(:,2);
segmentTurn = find(diff(segID_annID(:,1))>=1);
uniqueSegID = unique(segID_annID(:,1));
segID_annNum = [uniqueSegID zeros(length(uniqueSegID),1)];
for i = 1:length(uniqueSegID)
    begIdx = [1;(segmentTurn+1)]; endIdx = [segmentTurn;length(annID)];
    segID_annNum(i,2) = length(unique(annID(begIdx(i):endIdx(i))));
end

% pick only the ones that are annotated by 5 or more
segID_annNum(segID_annNum(:,2)<=4,:) = [];
segID = segID_annNum(:,1);

% get the final "event" table (str2 double, sort, class-conversion, onset/offset control)
event = arranged(:,[3 4 5 6 13]);
event = sortrows(event,2);
event = sortrows(event,1);
class = unique(event(:,end));
for i = 1:length(event(:,end))
    for j = 1:length(class)
        if strcmp(class(j),event(i,end))
            event(i,end) = {num2str(j)};
        end
    end
end
event = str2double(event);
event(event(:,3)<1) = 1; % onset control
event(event(:,4)>max_ms) = max_ms; % offset control

display('step 3 completed');


% 4) plurality voting
for i = 1:length(segID) % iterate through each segment
    
    % load audio feature
    load (strcat(path2feature,'featurePerSeg.',num2str(segID(i)),'.mat'));
    
    % get annotations of corresponding segment
    eventPerSeg = event(event(:,1)==segID(i),:);
    annIdPerSeg = unique(eventPerSeg(:,2));
    annNumPerSeg = length(annIdPerSeg);
    
    % make 'monophonic GT in polyphonic' for each annotation & vote
    hFig = figure(1);
    voteAcc = zeros(length(class),max_fr); 
    for j = 1:annNumPerSeg
        % get event per annotator in ms
        eventPerAnn = eventPerSeg(eventPerSeg(:,2)==annIdPerSeg(j),:);
        % convert to frame level
        eventperAnn_fr = [eventPerAnn(:,5) ms2fr(eventPerAnn(:,3:4),max_ms,max_fr)]; % events are not continuous
        % change to (1) monophonic roll (2) event form (3) polyphonic form
        temp1 = index2roll_mono(eventperAnn_fr,max_fr);
        temp2 = roll2index_mono(temp1); temp2(temp2(:,1)==0,:)=[];
        rollPerAnn = index2roll_poly(temp2,max_fr,length(class));
        % accumulate votiing
        voteAcc = voteAcc + rollPerAnn;
        % visualize in the mean time        
        subplot(3,3,j);
        imagesc(time,1:16,rollPerAnn);
        colormap(flipud(gray));
        set(gca,'YTick',1:length(class),'YTickLabel',class,'TickLength',[0 0]);
        title(strcat('ann.',num2str(annIdPerSeg(j))));
    end
    [maxVal,maxIdx] = max(voteAcc,[],1); % plurality voting
    maxVal = maxVal >= 2; % considering only the ones that are higher than 2...
    maxVal = maxVal + 0; % logic to double
    voteRoll = maxIdx.*maxVal;
    voteRoll = medfilt1(voteRoll,3); % 101 ms --> 3 frames    
    
    % frame_level design X & save
    monoGT_plurality_frameLevel_X_seg = [featurePerSeg voteRoll'];
    filename = strcat(path2save_frame,'monoGT_plurality_frameLevel_X_',num2str(segID(i)),'.mat');
    save(filename,'monoGT_plurality_frameLevel_X_seg');
    
    % event_level design X
    voteEvent = roll2index_mono(voteRoll); voteEvent(voteEvent(:,1)==0,:) = [];
    featureSumPerEvent = zeros(size(voteEvent,1),num_feature);
    for j = 1:size(voteEvent,1)
        featureSumPerEvent(j,:) = mean(featurePerSeg(voteEvent(j,2):voteEvent(j,3),:),1);
    end
    monoGT_plurality_eventLevel_X_seg = [featureSumPerEvent voteEvent(:,1)];
    filename = strcat(path2save_event,'monoGT_plurality_eventLevel_X_',num2str(segID(i)),'.mat');
    save(filename,'monoGT_plurality_eventLevel_X_seg');
    
    % finalize visualization
    subplot(3,3,9);
    imagesc(time,1:16,index2roll_poly(voteEvent,max_fr,length(class)));
    colormap(flipud(gray));
    set(gca,'YTick',1:length(class),'YTickLabel',class,'TickLength',[0 0]);
    title(strcat('seg.',num2str(segID(i))));
    set(hFig,'Position',[100 0 1300 900]);
    
    % save
    filename = strcat(path2save_fig,'monoGT_plurality_',num2str(segID(i)),'.fig');
    saveas(gcf,filename);
    
    close all;
end

display('step 4 completed');

