% 2016/10/11
% Jaeseong You
% New York University

% Binary ground truth based on majority
%   (1) event-level design matrix
%   (2) frame-level design matrix
%   (3) visualization

% close and clear
close all; clear; clc;

% 0) add necessary functions to path
addpath('..\_functions\roll_index');
addpath('..\_functions\external');


% 1) path set-up
path2save_event = 'D:\AED_dev\gt_archive\binary\majority\eventLevel_X\';
path2save_frame = 'D:\AED_dev\gt_archive\binary\majority\frameLevel_X\';
path2save_fig = 'D:\AED_dev\gt_archive\binary\majority\figure\';
path2feature = 'D:\AED_dev\acousticScene_feature\';
dir2feature = dir(path2feature);
numFile = length(dir2feature)-2;

display('step 1 completed');


% 2) variables set-up
num_feature = 81; % check in the feature-rendering
fs = 44100;
max_ms = 120300;
max_fr = 2582; % featue matrix is (4096 x 2582)
time = (1:max_ms)/1000;

display('step 2 completed');


% 3) prepare the annotation data
% read in raw annotation data & rearrange
fid = fopen('D:\AED_dev\rawData_archive\events_rendered.csv');
raw = textscan(fid,'%s','delimiter',',');
fclose(fid);

% rearrange & remove 'scene's
arranged = reshape(raw{1,1},13,[])';
arranged(strcmp(arranged(:,13),'scene'),:)=[];

% figure out the # of annotators per segment
segID_annID = sortrows(str2double(arranged(:,3:4)),1);
annID = segID_annID(:,2);
segmentTurn = find(diff(segID_annID(:,1))>=1);
uniqueSegID = unique(segID_annID(:,1));
segID_annNum = [uniqueSegID zeros(length(uniqueSegID),1)];
for i = 1:length(uniqueSegID)
    begIdx = [1;(segmentTurn+1)]; endIdx = [segmentTurn;length(annID)];
    segID_annNum(i,2) = length(unique(annID(begIdx(i):endIdx(i))));
end

% pick only the ones that are annotated by 5 or more
segID_annNum(segID_annNum(:,2)<=4,:) = [];
segID = segID_annNum(:,1);

% get the final "event" table (str2 double, sort, class-conversion, onset/offset control)
event = str2double(arranged(:,3:6));
event = sortrows(event,2);
event = sortrows(event,1);
event(event(:,3)<1) = 1; % onset control
event(event(:,4)>max_ms) = max_ms; % offset control

display('step 3 completed');


% 4) maority voting!
for i = 1:length(segID) % iterate through each segement
    
    % load audio feature
    load (strcat(path2feature,'featurePerSeg.',num2str(segID(i)),'.mat'));
    
    % get annotations of corresponding segment
    eventPerSeg = event(event(:,1)==segID(i),:);
    annIdPerSeg = unique(eventPerSeg(:,2));
    annNumPerSeg = length(annIdPerSeg);
    
    % getting the roll-form GT per seg
    vote_ms = zeros(annNumPerSeg,max_ms);
    vote_fr = zeros(annNumPerSeg,max_fr);
    for j = 1:annNumPerSeg
        eventPerAnn_ms = eventPerSeg(eventPerSeg(:,2)==annIdPerSeg(j),3:4);
        offset_fr = ms2fr(eventPerAnn_ms(:,2),max_ms,max_fr);
        onset_fr = [1;offset_fr(1:end-1)+1];
        eventperAnn_fr = [onset_fr offset_fr];
        vote_ms(j,:) = index2roll_binary(eventPerAnn_ms,max_ms);
        vote_fr(j,:) = index2roll_binary(ms2fr(eventPerAnn_ms,max_ms,max_fr),max_fr); 
    end
    
    % voting
    voteAcc_ms = sum(vote_ms)>(annNumPerSeg/2); voteAcc_ms = voteAcc_ms+0; 
    voteAcc_fr = sum(vote_fr)>(annNumPerSeg/2); voteAcc_fr = voteAcc_fr+0;
    
    % median pass
    voteAcc_ms = medfilt1(voteAcc_ms,101); % 101 ms
    voteAcc_fr = medfilt1(voteAcc_fr,3); % 101 ms --> 3 frames
    
    % frame_level design X & save
    binaryGT_majority_frameLevel_X_seg = [featurePerSeg voteAcc_fr'];
    filename = strcat(path2save_frame,'binaryGT_majority_frameLevel_X_',num2str(segID(i)),'.mat');
    save(filename,'binaryGT_majority_frameLevel_X_seg');
        
    % event_level design X
    voteEvent_fr = roll2index_mono(voteAcc_fr);
    featureSumPerEvent = zeros(size(voteEvent_fr,1),num_feature);
    for j = 1:size(voteEvent_fr,1)
        featureSumPerEvent(j,:) = mean(featurePerSeg(voteEvent_fr(j,2):voteEvent_fr(j,3),:),1);
    end
    binaryGT_majority_eventLevel_X_seg = [featureSumPerEvent voteEvent_fr(:,1)];
    filename = strcat(path2save_event,'binaryGT_majority_eventLevel_X_',num2str(segID(i)),'.mat');
    save(filename,'binaryGT_majority_eventLevel_X_seg');
    
    % visualize
    h = figure(1);
    for j = 1:annNumPerSeg
        g = subplot(annNumPerSeg+1,1,j);
        imagesc(time,1,vote_ms(j,:));
        colormap(flipud(gray));
        rowname = strcat('ann.',num2str(annIdPerSeg(j)));
        set(gca,'YTick',1,'YTickLabel',{rowname},'TickLength',[0 0]);
        p = get(g,'position'); p(4) = p(4)*.4; set(g,'position',p); % height control
    end
    g = subplot(annNumPerSeg+1,1,annNumPerSeg+1);
    imagesc(time,1,voteAcc_ms);
    colormap(flipud(gray));
    rowname = strcat('seg.',num2str(segID(i)));
    set(gca,'YTick',1,'YTickLabel',{rowname},'TickLength',[0 0]);
    p = get(g,'position'); p(4) = p(4)*.4; set(g,'position',p); % height control    
    
    % save
    filename = strcat('binaryGT_majority_',num2str(segID(i)),'.fig');
    set(h,'Position',[300 100 500 100*(annNumPerSeg+1)]);
    saveas(gcf,strcat(path2save_fig,filename));
    
    % figure close
    close all;
end

display('step 4 completed');

