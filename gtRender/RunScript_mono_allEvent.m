% 2016/10/15
% Jaeseong You
% New York University

% Monophonic ground truth based on all events
%   (1) event-level design matrix
%   (2) frame-level design matrix

% close and clear
close all; clear; clc;

% 0) add necessary functions to path
addpath('..\_functions\roll_index');
addpath('..\_functions\external');


% 1) path set-up
path2save = 'D:\AED_dev\';
path2save_event = 'D:\AED_dev\gt_archive\mono\allEvent\eventLevel_X\';
path2save_frame = 'D:\AED_dev\gt_archive\mono\allEvent\frameLevel_X\';
path2feature = 'D:\AED_dev\acousticScene_feature\';
dir2feature = dir(path2feature);
numFile = length(dir2feature)-2;

display('step 1 completed');


% 2) variables set-up
num_feature = 81; % check in the feature-rendering
fs = 44100;
max_ms = 120300;
max_fr = 2582; % feature matrix is (4096 x 2582)
time = (1:max_ms)/1000;

display('step 2 completed');


% 3) prepare the annotation data
% read in raw annotation data & rearrange
fid = fopen('D:\AED_dev\rawData_archive\events_rendered.csv');
raw = textscan(fid,'%s','delimiter',',');
fclose(fid);

% rearrange & remove 'scene's
arranged = reshape(raw{1,1},13,[])';
arranged(strcmp(arranged(:,13),'scene'),:)=[];

% figure out the # of annotators per segment
segID_annID = sortrows(str2double(arranged(:,3:4)),1);
annID = segID_annID(:,2);
segmentTurn = find(diff(segID_annID(:,1))>=1);
uniqueSegID = unique(segID_annID(:,1));
segID_annNum = [uniqueSegID zeros(length(uniqueSegID),1)];
for i = 1:length(uniqueSegID)
    begIdx = [1;(segmentTurn+1)]; endIdx = [segmentTurn;length(annID)];
    segID_annNum(i,2) = length(unique(annID(begIdx(i):endIdx(i))));
end

% pick only the ones that are annotated by 5 or more
segID_annNum(segID_annNum(:,2)<=4,:) = [];
segID = segID_annNum(:,1);

% get the final "event" table (str2 double, sort, class-conversion, onset/offset control)
event = arranged(:,[3 4 5 6 13]);
event = sortrows(event,2);
event = sortrows(event,1);
class = unique(event(:,end));
for i = 1:length(event(:,end))
    for j = 1:length(class)
        if strcmp(class(j),event(i,end))
            event(i,end) = {num2str(j)};
        end
    end
end
event = str2double(event);
event(event(:,3)<1) = 1; % onset control
event(event(:,4)>max_ms) = max_ms; % offset control

display('step 3 completed');


% 4) all-event GT computation
for i = 1:length(segID) % iterate through each segment
    
    % load audio feature
    load (strcat(path2feature,'featurePerSeg.',num2str(segID(i)),'.mat'));
    
    % get annotations of corresponding segment
    eventPerSeg = event(event(:,1)==segID(i),:);
    annIdPerSeg = unique(eventPerSeg(:,2));
    annNumPerSeg = length(annIdPerSeg);
    
    % make 'monophonic GT in polyphonic' for each annotation & vote
    rollAnns = zeros(annNumPerSeg,max_fr); 
    eventAnns = [];
    for j = 1:annNumPerSeg
        % get event per annotator in ms
        eventPerAnn = eventPerSeg(eventPerSeg(:,2)==annIdPerSeg(j),:);
        % convert to frame level
        eventperAnn_fr = [eventPerAnn(:,5) ms2fr(eventPerAnn(:,3:4),max_ms,max_fr)]; % events are not continuous
        % change to (1) monophonic roll (2) event form (3) polyphonic form
        rollPerAnn = index2roll_mono(eventperAnn_fr,max_fr);
        rollAnns(j,:) = medfilt1(rollPerAnn,3);
        eventAnns = [eventAnns;roll2index_mono(rollAnns(j,:))];
    end

    % frame_level design X & save
    monoGT_allEvent_frameLevel_X_seg = [featurePerSeg rollAnns'];
    filename = strcat(path2save_frame,'monoGT_allEvent_frameLevel_X_',num2str(segID(i)),'.mat');
    save(filename,'monoGT_allEvent_frameLevel_X_seg');
    
    % event_level design X & save
    eventAnns(eventAnns(:,1)==0,:) = [];
    featureSumPerEvent = zeros(size(eventAnns,1),num_feature);
    for j = 1:size(eventAnns,1)
        featureSumPerEvent(j,:) = mean(featurePerSeg(eventAnns(j,2):eventAnns(j,3),:),1);
    end
    monoGT_allEvent_eventLevel_X_seg = [featureSumPerEvent eventAnns(:,1)];
    filename = strcat(path2save_event,'monoGT_allEvent_eventLevel_X_',num2str(segID(i)),'.mat');
    save(filename,'monoGT_allEvent_eventLevel_X_seg');
    
end

display('step 4 completed');

