% Resynthesis

% 2016/10/03
% Jaesenog You
% New York University

% clear and close
close all;
clear;
clc;

% add necessary functions to path
addpath('C:\Users\jaese\OneDrive\Documents\MATLAB\AED\_functions\roll_index');

% parameters
max_ms = 120300;
fs = 44100;
max_fs = max_ms/1000*fs;

% detection types
aedType = 1;
if aedType == 1 
    % mono detection
    %   binary detection trained with majority 
    %   classification trained with plurality
    pathAED = 'D:\AED_repository\result_detection\mono\1_1\'; 
    pathMapping = 'D:\AED_repository\result_mapping\mono\1\';
    pathSave = 'D:\AED_repository\result_resynthesis\mono\1\';
elseif aedType == 2
    % mono detection
    %   binary detection trained with eventness 
    %   classification trained with plurality
    pathAED = 'D:\AED_repository\result_detection\mono\1_2\';
    pathMapping = 'D:\AED_repository\result_mapping\mono\2\';
    pathSave = 'D:\AED_repository\result_resynthesis\mono\2\';
elseif aedType == 3
    % mono detection
    %   binary detection trained with raw annotation 
    %   classification trained with plurality
    pathAED = 'D:\AED_repository\result_detection\mono\1_3\';
    pathMapping = 'D:\AED_repository\result_mapping\mono\3\';
    pathSave = 'D:\AED_repository\result_resynthesis\mono\3\';
elseif aedType == 4
    % mono detection
    %   binary detection trained with majority 
    %   classification trained with raw annotation
    pathAED = 'D:\AED_repository\result_detection\mono\1_4\';
    pathMapping = 'D:\AED_repository\result_mapping\mono\4\';
    pathSave = 'D:\AED_repository\result_resynthesis\mono\4\';
elseif aedType == 5
    % mono detection
    %   binary detection trained with eventness 
    %   classification trained with raw annotation
    pathAED = 'D:\AED_repository\result_detection\mono\1_5\';
    pathMapping = 'D:\AED_repository\result_mapping\mono\5\';
    pathSave = 'D:\AED_repository\result_resynthesis\mono\5\';
elseif aedType == 6 %
    % mono detection
    %   binary detection trained with raw anotation 
    %   classification trained with raw annotation
    pathAED = 'D:\AED_repository\result_detection\mono\1_6\';
    pathMapping = 'D:\AED_repository\result_mapping\mono\6\';
    pathSave = 'D:\AED_repository\result_resynthesis\mono\6\';
end
% add more as polyphonic AED developed
dirAED = dir(pathAED);
dirMapping = dir(pathMapping);

% iterate each segment
numSegment = length(dirMapping)-2;
for i = 3:length(dirMapping)
    
    % getting segment ID
    splitname = strsplit(dirMapping(i).name,'.');
    segmentID = char(splitname(2));
    display(segmentID);
    
    % load the aed result
    load(strcat(pathAED,'classificatinoPerSeg.',segmentID,'.mat'));
    
    % load the mapping result
    load(strcat(pathMapping,'mappingPerSeg.',segmentID,'.mat'));
    
    % iterate detected events of the current segment
    resynthPerSeg = zeros(max_fs,1);
    numEvent = length(mappingPerSeg);
    for j = 1:numEvent
        % get the corresponding audio per detected event
        [x,~] = audioread(char(mappingPerSeg{j}));
        if size(x,2) == 2
            x = mean(x,2);
        end
        x = x-mean(x);
        
        % adjust the loudness of each sample
        
        % adding samples to the output
        onset = ms2fs(classificationPerSeg(j,2),fs);
        sampleDur = length(x);
        offset = onset+sampleDur-1;
        if offset > max_fs
            resynthPerSeg(onset:max_fs) = resynthPerSeg(onset:max_fs)+x(1:max_fs-onset+1);
        else
            resynthPerSeg(onset:offset) = resynthPerSeg(onset:offset)+x;
        end

    end
            
    % save
    resynthPerSeg = resynthPerSeg/max(abs(resynthPerSeg));
    filename = strcat('resynthPerSeg.',segmentID,'.wav');
    audiowrite(strcat(pathSave,filename),resynthPerSeg,fs);
    
end

