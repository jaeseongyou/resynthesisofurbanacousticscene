% Mapping
%   reads detection results
%   outputs the mapped results
%   *make sure to run 'RunScript_computeAudioSampleFeature.m' beforehand
% 2016/10/02
% Jaeseong You
% New York University

% clean and close
clear;
clc;
close all;

% path and dir
pathSamplePool_feature = 'D:\AED_repository\audioSample_feature\';
dirSamplePool_feature = dir(pathSamplePool_feature);
pathSamplePool_audio = 'D:\AED_repository\audioSample_raw\';
pathSegment_feature = 'D:\AED_repository\acousticScene_feature\';

% detection types
aedType = 1;
if aedType == 1 
    % mono detection
    %   binary detection trained with majority 
    %   classification trained with plurality
    pathAED = 'D:\AED_repository\result_detection\mono\1_1\'; 
    pathSave = 'D:\AED_repository\result_mapping\mono\1\';
elseif aedType == 2
    % mono detection
    %   binary detection trained with eventness 
    %   classification trained with plurality
    pathAED = 'D:\AED_repository\result_detection\mono\1_2\';
    pathSave = 'D:\AED_repository\result_mapping\mono\2\';
elseif aedType == 3
    % mono detection
    %   binary detection trained with raw annotation 
    %   classification trained with plurality
    pathAED = 'D:\AED_repository\result_detection\mono\1_3\';
    pathSave = 'D:\AED_repository\result_mapping\mono\3\';
elseif aedType == 4
    % mono detection
    %   binary detection trained with majority 
    %   classification trained with raw annotation
    pathAED = 'D:\AED_repository\result_detection\mono\1_4\';
    pathSave = 'D:\AED_repository\result_mapping\mono\4\';
elseif aedType == 5
    % mono detection
    %   binary detection trained with eventness 
    %   classification trained with raw annotation
    pathAED = 'D:\AED_repository\result_detection\mono\1_5\';
    pathSave = 'D:\AED_repository\result_mapping\mono\5\';
elseif aedType == 6 %
    % mono detection
    %   binary detection trained with raw anotation 
    %   classification trained with raw annotation
    pathAED = 'D:\AED_repository\result_detection\mono\1_6\';
    pathSave = 'D:\AED_repository\result_mapping\mono\6\'; 
end
% add more as polyphonic AED developed
dirAED = dir(pathAED);

% iterate through each segment
numSegment = length(dirAED)-3; % excluding the figure directory
for i = 1:numSegment

    % load classificationPerSeg
    %   row 1: class
    %   row 2 and 3: onset offset (in ms)
    %   row 4 and 5: onset offset (in frame index)
    load(strcat(pathAED,dirAED(i+2).name)); % classificationPerSeg loaded
    splitname = strsplit(dirAED(i+2).name,'.');
    segmentID = char(splitname(2));
    display(strcat(segmentID,': aed result loaded'));
    
    % load featurePerSeg
    load(strcat(pathSegment_feature,'featurePerSeg.',segmentID,'.mat'));
    display(strcat(segmentID,': audio feature loaded'));
    
    % iterate through detected events in the current segment
    numEvent = size(classificationPerSeg,1);
    mappingPerSeg = cell(numEvent,1);
    for j = 1:numEvent
        % getting the feature averaged across frames per event
        featurePerEvent = mean(featurePerSeg(classificationPerSeg(j,4):classificationPerSeg(j,5),:),1);
        
        %finding a matching sample pool
        classID = classificationPerSeg(j,1);
        for k = 3:length(dirSamplePool_feature) % iterating pool folders to find a matching ID
            samplePoolName = dirSamplePool_feature(k).name;
            splitname = strsplit(samplePoolName,'_');
            if classID == str2double(splitname(1));
                chosenPool = samplePoolName;
                pathChosenPool = strcat(pathSamplePool_feature,chosenPool,'\');
                dirChosenPool = dir(pathChosenPool);
                display(['a matching pool ', chosenPool, ' found.']); 
            end
        end
        
        % load the sample features
        numSample = length(dirChosenPool)-2;
        featureComposite = zeros(numSample+1,81); % detected event & samples of the correspondin class
        for k= 3:length(dirChosenPool)
            %load featurePerSample
            load(strcat(pathChosenPool,dirChosenPool(k).name));
            featureComposite(k-1,:) = featurePerSample; % first row is for the detected event
        end
        featureComposite(1,:) = featurePerEvent;
        
        % normalize features and compute the euclidean distance
        zFeatureComposite = zscore(featureComposite); %normalizing each column
        eDistance = bsxfun(@minus,zFeatureComposite(2:end,:),zFeatureComposite(1,:));  
        eDistance = sqrt(sum(eDistance.^2,2));
        
        % choose the min distance
        [~,minID] = min(eDistance);
        minDistanceSample = dirChosenPool(minID+2).name;
        [~,filename,~] = fileparts(minDistanceSample);
        mappingPerSeg{j,1} = strcat(pathSamplePool_audio,chosenPool,'\',filename,'.wav');
        display([filename, ' selected']);   
    end
    
    filename = strcat(pathSave,'mappingPerSeg.',segmentID,'.mat');
    save(filename,'mappingPerSeg');
    
end

