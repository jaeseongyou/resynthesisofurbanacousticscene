% 2016/10/16
% Jaeseong You
% New York University

% Mono binary detection using frame-level GT
%   (1) detection result
%   (2) evaluation
%   (3) visualization

% clear and close
close all; clear; clc;

% parameters (global)
fs = 44100;
max_ms = 120300;
max_fr = 2582; % feature matrix is (4096 x 2582)
max_fs = fs*max_ms/1000;
time = (1:max_fr)/max_fr*120.3;

% add necessary functions to path
addpath('..\..\..\_functions\roll_index');
addpath('..\..\..\_functions\external');
addpath('..\..\..\_functions\evaluation');

% post-processing variables
threshold_pad = fs2fr(1*fs,max_fs,max_fr);
threshold_silence = fs2fr(.5*fs,max_fs,max_fr);
threshold_event = fs2fr(.2*fs,max_fs,max_fr);
pad_front = fs2fr(.2*fs,max_fs,max_fr);
pad_back = fs2fr(.4*fs,max_fs,max_fr);


% 1) path set-up

typeGT = 'majority';
if strcmp(typeGT,'majority')
    pathVariable = 'D:\AED_dev\result_detection\monoBinary\majority\frameLevel\';
    path2gt = 'D:\AED_dev\gt_archive\binary\majority\frameLevel_X\';
elseif strcmp(typeGT,'eventness')
    pathVariable = 'D:\AED_dev\result_detection\monoBinary\eventness\frameLevel\';
    path2gt = 'D:\AED_dev\gt_archive\binary\eventness\frameLevel_X\';
elseif strcmp(typeGT,'allEvent')
    pathVariable = 'D:\AED_dev\result_detection\monoBinary\allEvent\frameLevel\';
    path2gt = 'D:\AED_dev\gt_archive\binary\allEvent\frameLevel_X\';
end
dir2gt = dir(path2gt);
path2save_detect = strcat(pathVariable,'detection\'); 
path2save_eval = strcat(pathVariable,'evaluation\');
path2save_fig = strcat(pathVariable,'figure\');


% 2) Prepare training & test data

% randomize (seed)
rng('default');rng(1);
randSeq = randperm(length(dir2gt)-2)+2;

% k-fold
k = 5;
kSize = round(length(randSeq)/k);
cMatrix_post = zeros(2);
cMatrix_masked = zeros(2);
cMatrix_raw = zeros(2);
for i = 1:k
    display(strcat('fold-', num2str(i)));
    
    % divide into test and train
    randIdx = randSeq;
    if i == 5 % last turn control
        testID = randIdx((i-1)*kSize+1:end);
        randIdx((i-1)*kSize+1:end)=[];
    else
        testID = randIdx((i-1)*kSize+1:i*kSize);
        randIdx((i-1)*kSize+1:i*kSize)=[];
    end 
    trainID = randIdx;
    
    % 2_1) TRAINING
    % get the training data
    trainX = []; trainY = [];
    for j = 1:length(trainID)
        filename = dir2gt(trainID(j)).name;
        load(strcat(path2gt,filename));
        if strcmp(typeGT,'majority')
            trainX = [trainX;binaryGT_majority_frameLevel_X_seg(:,1:end-1)];
            trainY = [trainY;binaryGT_majority_frameLevel_X_seg(:,end)];
        elseif strcmp(typeGT,'eventness')
            trainX = [trainX;binaryGT_eventness_frameLevel_X_seg(:,1:end-1)];
            trainY = [trainY;binaryGT_eventness_frameLevel_X_seg(:,end)];
        elseif strcmp(typeGT,'allEvent')
            trainX = [trainX;binaryGT_allEvent_frameLevel_X_seg(:,1:81)]; % features only
            trainY = [trainY;binaryGT_allEvent_frameLevel_X_seg(:,end)];
        end
    end
    
    % training
    tic
    display('training begins...');
    nTrees = 500;
    rfModel = TreeBagger(nTrees, trainX, trainY, 'Method', 'classification');
    display('training done!');
    toc
    
    % 2_2) TEST
    for j = 1:length(testID)
        % load each design X per segment
        filename = dir2gt(testID(j)).name;
        load(strcat(path2gt,filename));
        if strcmp(typeGT,'majority')
            testX = binaryGT_majority_frameLevel_X_seg(:,1:end-1);
            testY = binaryGT_majority_frameLevel_X_seg(:,end);
        elseif strcmp(typeGT,'eventness')
            testX = binaryGT_eventness_frameLevel_X_seg(:,1:end-1);
            testY = binaryGT_eventness_frameLevel_X_seg(:,end);
        elseif strcmp(typeGT,'allEvent')
            testX = binaryGT_allEvent_frameLevel_X_seg(:,1:81);
            testY = binaryGT_allEvent_frameLevel_X_seg(:,end);
        end
        
        % get seg ID
        [~,filenameOnly,~] = fileparts(filename);
        splitname = strsplit(filenameOnly,'_');
        segID = char(splitname(5));
        
        % detection
        rawDetection = str2double(rfModel.predict(testX));
        display(strcat('detection done for seg',segID));
        
        % post processing
        %rms masking
        rmsVal = testX(:,1);
        rmsThreshold = movmean(rmsVal,323);
        rmsDetection = rmsVal > rmsThreshold; rmsDetection = rmsDetection + 0;
        maskedDetection = rawDetection.*rmsDetection;
        %rms adding
        rmsAddition = zscore(rmsVal) > 1.645; % larger than confidence interval of 90%
        maskedDetection = maskedDetection+rmsAddition;
        maskedDetection = maskedDetection > 0; maskedDetection = maskedDetection + 0;
        %convert to event format
        detectionID = roll2index_mono(maskedDetection);
        eventDur = detectionID(:,3)-detectionID(:,2)+1;
        % remove short silence ( < .5 second = )
        for k = 1:length(eventDur)
            if detectionID(k,1)==0 && eventDur(k)<threshold_silence
                detectionID(k,1) = 1;
            end
        end
        %express as event format once more
        detectionRoll = index2roll_mono(detectionID,max_fr);
        detectionID = roll2index_mono(detectionRoll);
        eventDur = detectionID(:,3)-detectionID(:,2)+1;
        % remove short event ( < .1 second)
        for k = 1:length(eventDur)
            if detectionID(k,1)==1 && eventDur(k)<threshold_event
                detectionID(k,1) = 0;
            end
        end
        % express as event format once more
        detectionRoll = index2roll_mono(detectionID,max_fr);
        detectionID = roll2index_mono(detectionRoll);
        eventDur = detectionID(:,3)-detectionID(:,2)+1;
        % pad front (.2 second) and end (.4 second)
        for k = 1:length(eventDur)
            if eventDur(k) < threshold_pad
                if detectionID(k,1)==1
                    if k==1
                        % back pad
                        detectionID(k,3) = detectionID(k,3)+pad_back;
                        detectionID(k+1,2) = detectionID(k+1,2)+pad_back;
                    elseif k == length(eventDur)
                        % front pad
                        detectionID(k-1,3) = detectionID(k-1,3)-pad_front;
                        detectionID(k,2) = detectionID(k,2)-pad_front;
                    else
                        % front pad
                        detectionID(k-1,3) = detectionID(k-1,3)-pad_front;
                        detectionID(k,2) = detectionID(k,2)-pad_front;
                        % back pad
                        detectionID(k,3) = detectionID(k,3)+pad_back;
                        detectionID(k+1,2) = detectionID(k+1,2)+pad_back;
                    end
                end
            end
        end
        % remove short silence once more
        for k = 1:length(eventDur)
            if detectionID(k,1)==0 && eventDur(k)<threshold_silence
                detectionID(k,1) = 1;
            end
        end
        % final conversion
        detectionPerSeg = index2roll_mono(detectionID,max_fr);
        display(strcat('post-processing done for seg',segID));
        
        % save detection
        filename = strcat(path2save_detect,'detectionPerSeg.',segID,'.mat');
        save(filename,'detectionPerSeg');
        
        % visualization (GT, initial, final)
        h = figure(1);
        g = subplot(3,1,1); % GT
        imagesc(time,1,testY');
        colormap(flipud(gray));
        set(gca,'YTick',1,'YTickLabel',{'GT'},'TickLength',[0 0]);
        p = get(g,'position'); p(4) = p(4)*.4; set(g,'position',p); % height control
        title(strcat('seg.',segID));
        
        g = subplot(3,1,2); % initial raw detection
        imagesc(time,1,rawDetection');
        colormap(flipud(gray));
        set(gca,'YTick',1,'YTickLabel',{'raw'},'TickLength',[0 0]);
        p = get(g,'position'); p(4) = p(4)*.4; set(g,'position',p); % height control    
        
        g = subplot(3,1,3); % final detection
        imagesc(time,1,detectionPerSeg);
        colormap(flipud(gray));
        set(gca,'YTick',1,'YTickLabel',{'final'},'TickLength',[0 0]);
        p = get(g,'position'); p(4) = p(4)*.4; set(g,'position',p); % height control    
        
        figurename = strcat(path2save_fig,'detectionPerSeg.',segID,'.fig');
        saveas(gcf,figurename);
        
        % visualization (Expansion)
        h2 = figure(2);         
        
        g = subplot(3,2,1); % initial raw detection
        imagesc(time,1,rawDetection');
        colormap(flipud(gray));
        set(gca,'YTick',1,'YTickLabel',{'raw'},'TickLength',[0 0]);
        p = get(g,'position'); p(4) = p(4)*.4; set(g,'position',p); % height control
               
        g = subplot(3,2,3); % RMS detection
        imagesc(time,1,rmsDetection');
        colormap(flipud(gray));
        set(gca,'YTick',1,'YTickLabel',{'rms'},'TickLength',[0 0]);
        p = get(g,'position'); p(4) = p(4)*.4; set(g,'position',p); % height control   
        
        g = subplot(3,2,5); % masked
        imagesc(time,1,maskedDetection');
        colormap(flipud(gray));
        set(gca,'YTick',1,'YTickLabel',{'masked'},'TickLength',[0 0]);
        p = get(g,'position'); p(4) = p(4)*.4; set(g,'position',p); % height control         
        
        subplot(3,2,2); % rms & threholding
        plot(time,rmsVal); hold on;
        plot(time,rmsThreshold); hold off;
        xlim([0 120.3]);

        g = subplot(3,2,4); % GT
        imagesc(time,1,testY');
        colormap(flipud(gray));
        set(gca,'YTick',1,'YTickLabel',{'GT'},'TickLength',[0 0]);
        p = get(g,'position'); p(4) = p(4)*.4; set(g,'position',p); % height control
        title(strcat('seg.',segID));        

        g = subplot(3,2,6); % final detection
        imagesc(time,1,detectionPerSeg);
        colormap(flipud(gray));
        set(gca,'YTick',1,'YTickLabel',{'final'},'TickLength',[0 0]);
        p = get(g,'position'); p(4) = p(4)*.4; set(g,'position',p); % height control    
        
        set(h2,'Position',[200 100 800 550]); % x position, y position, width, height
        figurename = strcat(path2save_fig,'detectionPerSeg.EP.',segID,'.fig');
        saveas(gcf,figurename);
        
        % accumulate evaluation
        cMatrix_raw = cMatrix_raw + confusionmat(testY,rawDetection);
        cMatrix_masked = cMatrix_masked + confusionmat(testY,maskedDetection);
        cMatrix_post = cMatrix_post + confusionmat(testY,detectionPerSeg);
        close all;
    end   
end
% finalize & save evaluation
eval_raw = evalMetricsBinary(cMatrix_raw);
eval_masked = evalMetricsBinary(cMatrix_masked);
eval_post = evalMetricsBinary(cMatrix_post);
filename = strcat(path2save_eval,'evaluation_raw.mat');
save(filename,'eval_raw');
filename = strcat(path2save_eval,'evaluation_masked.mat');
save(filename,'eval_masked');
filename = strcat(path2save_eval,'evaluation_post.mat');
save(filename,'eval_post');

