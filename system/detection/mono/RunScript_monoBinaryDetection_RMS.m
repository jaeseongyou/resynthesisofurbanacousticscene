% 2016/10/16
% Jaeseong You
% New York University

% Mono binary detection using RMS
%   (1) detection result
%   (2) evaluation
%   (2) visualization

% clear and close
close all; clear; clc;

% parameters (global)
fs = 44100;
max_ms = 120300;
max_fr = 2582; % feature matrix is (4096 x 2582)
max_fs = fs*max_ms/1000;
time = (1:max_fr)/max_fr*120.3;

% add necessary functions to path
addpath('..\..\..\_functions\roll_index');
addpath('..\..\..\_functions\external');
addpath('..\..\..\_functions\evaluation');

% post-processing variables
threshold_pad = fs2fr(1*fs,max_fs,max_fr);
threshold_silence = fs2fr(.5*fs,max_fs,max_fr);
threshold_event = fs2fr(.2*fs,max_fs,max_fr);
pad_front = fs2fr(.2*fs,max_fs,max_fr);
pad_back = fs2fr(.4*fs,max_fs,max_fr);


% 1) path set-up
path2majority = 'D:\AED_dev\gt_archive\binary\majority\frameLevel_X\'; % feature\visualization\evaluation purpose
dir2majority = dir(path2majority);
path2save_detect = 'D:\AED_dev\result_detection\monoBinary\rms\detection\';
path2save_eval = 'D:\AED_dev\result_detection\monoBinary\rms\evaluation\';
path2save_fig = 'D:\AED_dev\result_detection\monoBinary\rms\figure\';

% 2) rms thresholding
cMatrix_raw = zeros(2);
cMatrix_post = zeros(2);
for i = 3:length(dir2majority)
    % load each design X per segment
    filename = dir2majority(i).name;
    load(strcat(path2majority,filename));
    X = binaryGT_majority_frameLevel_X_seg(:,1);
    Y = binaryGT_majority_frameLevel_X_seg(:,end);

    % get seg ID
    [~,filenameOnly,~] = fileparts(filename);
	splitname = strsplit(filenameOnly,'_');
	segID = char(splitname(5));
    
    % detection
    threshold = ones(max_fr,1)*mean(X)*1.645;
    %threshold = movmean(X,33)*1.28;
    rawDetection = X > threshold;
    rawDetection = rawDetection + 0;
    
    % post processing
    %convert to event format
    detectionID = roll2index_mono(rawDetection);
	eventDur = detectionID(:,3)-detectionID(:,2)+1;
	% remove short silence ( < .5 second = )
    for j = 1:length(eventDur)
        if detectionID(j,1)==0 && eventDur(j)<threshold_silence
            detectionID(j,1) = 1;
        end
    end
    
	%express as event format once more
	detectionRoll = index2roll_mono(detectionID,max_fr);
	detectionID = roll2index_mono(detectionRoll);
	eventDur = detectionID(:,3)-detectionID(:,2)+1;
	% remove short event ( < .1 second)
    for j = 1:length(eventDur)
        if detectionID(j,1)==1 && eventDur(j)<threshold_event
            detectionID(j,1) = 0;
        end
    end
	% express as event format once more
	detectionRoll = index2roll_mono(detectionID,max_fr);
	detectionID = roll2index_mono(detectionRoll);
	eventDur = detectionID(:,3)-detectionID(:,2)+1;
	% pad front (.2 second) and end (.4 second)
	for j = 1:length(eventDur)
        if eventDur(j) < threshold_pad
            if detectionID(j,1)==1
                if j==1
                    % back pad
                    detectionID(j,3) = detectionID(j,3)+pad_back;
                    detectionID(j+1,2) = detectionID(j+1,2)+pad_back;
                elseif j == length(eventDur)
                    % front pad
                    detectionID(j-1,3) = detectionID(j-1,3)-pad_front;
                    detectionID(j,2) = detectionID(j,2)-pad_front;
                else
                    % front pad
                    detectionID(j-1,3) = detectionID(j-1,3)-pad_front;
                    detectionID(j,2) = detectionID(j,2)-pad_front;
                    % back pad
                    detectionID(j,3) = detectionID(j,3)+pad_back;
                    detectionID(j+1,2) = detectionID(j+1,2)+pad_back;
                end
            end
        end
	end
	% remove short silence once more
	for j = 1:length(eventDur)
        if detectionID(j,1)==0 && eventDur(j)<threshold_silence
            detectionID(j,1) = 1;
        end
	end
	% final conversion
	detectionPerSeg = index2roll_mono(detectionID,max_fr);
	display(strcat('post-processing done for seg',segID));
        
	% save detection
	filename = strcat(path2save_detect,'detectionPerSeg.',segID,'.mat');
	save(filename,'detectionPerSeg');
        
	% visualization (GT & detection)
	h = figure(1);
	g = subplot(4,1,1); % GT
	imagesc(time,1,Y');
	colormap(flipud(gray));
	set(gca,'YTick',1,'YTickLabel',{'GT'},'TickLength',[0 0]);
	p = get(g,'position'); p(4) = p(4)*.4; set(g,'position',p); % height control
	title(strcat('seg.',segID));
    
	subplot(4,1,2); % initial raw detection
	plot(time,X); hold on;
    plot(time,threshold); hold off;
   	xlim([0 120.3]);
    
	g = subplot(4,1,3); % initial raw detection
	imagesc(time,1,rawDetection');
	colormap(flipud(gray));
	set(gca,'YTick',1,'YTickLabel',{'raw'},'TickLength',[0 0]);
	p = get(g,'position'); p(4) = p(4)*.4; set(g,'position',p); % height control    
        
	g = subplot(4,1,4); % final detection
	imagesc(time,1,detectionPerSeg);
	colormap(flipud(gray));
	set(gca,'YTick',1,'YTickLabel',{'detection'},'TickLength',[0 0]);
	p = get(g,'position'); p(4) = p(4)*.4; set(g,'position',p); % height control    
        
	figurename = strcat(path2save_fig,'detectionPerSeg.',segID,'.fig');
	saveas(gcf,figurename);
        
	% accumulate evaluation
	cMatrix_raw = cMatrix_raw + confusionmat(Y,rawDetection);
	cMatrix_post = cMatrix_post + confusionmat(Y,detectionPerSeg);
    close all;
end   

% finalize & save evaluation
eval_raw = evalMetricsBinary(cMatrix_raw);
eval_post = evalMetricsBinary(cMatrix_post);
filename = strcat(path2save_eval,'evaluation_raw.mat');
save(filename,'eval_raw');
filename = strcat(path2save_eval,'evaluation_post.mat');
save(filename,'eval_post');

