% 2016/10/17
% Jaeseong You
% New York University

% Mono AED
%   (1) detection result
%   (2) evaluation
%   (3) visualization

% clear and close
close all; clear; clc;

% parameters (global)
fs = 44100;
max_ms = 120300;
max_fr = 2582; % feature matrix is (4096 x 2582)
max_fs = fs*max_ms/1000;
time = (1:max_fr)/max_fr*120.3;
class = {'bird';'bus';'cough';'door';'engine';'footstep';'horn';'laugh';'metal';'motorcycle';'music';'other';'pass';'rustle';'stop';'voice'};

% add necessary functions to path
addpath('..\..\..\_functions\roll_index');
addpath('..\..\..\_functions\external');
addpath('..\..\..\_functions\evaluation');

% post-processing variables
threshold_pad = fs2fr(1*fs,max_fs,max_fr);
threshold_silence = fs2fr(.5*fs,max_fs,max_fr);
threshold_event = fs2fr(.2*fs,max_fs,max_fr);
pad_front = fs2fr(.2*fs,max_fs,max_fr);
pad_back = fs2fr(.4*fs,max_fs,max_fr);


% 1) type control

% mono train types
typeGT = 'plurality\eventLevel\';
if strcmp(typeGT,'plurality\frameLevel\')
    path2train = 'D:\AED_dev\gt_archive\mono\plurality\frameLevel_X\';
    path2test = 'D:\AED_dev\gt_archive\mono\plurality\frameLevel_X\';
elseif strcmp(typeGT,'plurality\eventLevel\')
    path2train = 'D:\AED_dev\gt_archive\mono\plurality\eventLevel_X\';
    path2test = 'D:\AED_dev\gt_archive\mono\plurality\frameLevel_X\';
elseif strcmp(typeGT,'allEvent\frameLevel\')
    path2train = 'D:\AED_dev\gt_archive\mono\allEvent\frameLevel_X\';
    path2test = 'D:\AED_dev\gt_archive\mono\allEvent\frameLevel_X\';
elseif strcmp(typeGT,'allEvent\eventLevel\')
    path2train = 'D:\AED_dev\gt_archive\mono\allEvent\eventLevel_X\';
    path2test = 'D:\AED_dev\gt_archive\mono\allEvent\frameLevel_X\';
end
dir2train = dir(path2train);
dir2test = dir(path2test);
% binary detection types
typeBinary = 'majority\frameLevel\';
if strcmp(typeBinary,'majority\frameLevel\')
    path2binary = 'D:\AED_dev\result_detection\monoBinary\majority\frameLevel\detection\';
elseif strcmp(typeBinary,'majority\eventLevel\')
    path2binary = 'D:\AED_dev\result_detection\monoBinary\majority\eventLevel\detection\';
elseif strcmp(typeBinary,'eventness\frameLevel\')
    path2binary = 'D:\AED_dev\result_detection\monoBinary\eventness\frameLevel\detection\';
elseif strcmp(typeBinary,'eventness\eventLevel\')
    path2binary = 'D:\AED_dev\result_detection\monoBinary\eventness\eventLevel\detection\';
elseif strcmp(typeBinary,'allEvent\frameLevel\')
    path2binary = 'D:\AED_dev\result_detection\monoBinary\allEvent\frameLevel\detection\';
elseif strcmp(typeBinary,'allEvent\eventLevel\')
    path2binary = 'D:\AED_dev\result_detection\monoBinary\allEvent\eventLevel\detection\';
end
dir2binary = dir(path2binary);

% 2) set up path
pathVariable = strcat('D:\AED_dev\result_detection\mono\',typeGT,typeBinary);
path2save_detect = strcat(pathVariable,'detection\'); 
path2save_eval = strcat(pathVariable,'evaluation\');
path2save_fig = strcat(pathVariable,'figure\');


% 3) Prepare training & test data

% randomize (seed)
rng('default');rng(1);
randSeq = randperm(length(dir2train)-2)+2;

% k-fold
k = 5;
kSize = round(length(randSeq)/k);
cMatrix_post = zeros(2);
cMatrix_raw = zeros(2);
for i = 1:k
    display(strcat('fold-', num2str(i)));
    
    % divide into test and train
    randIdx = randSeq;
    if i == 5 % last turn control
        testID = randIdx((i-1)*kSize+1:end);
        randIdx((i-1)*kSize+1:end)=[];
    else
        testID = randIdx((i-1)*kSize+1:i*kSize);
        randIdx((i-1)*kSize+1:i*kSize)=[];
    end 
    trainID = randIdx;
    
    % 3_1) TRAINING
    % get the training data
    trainX = []; trainY = [];
    for j = 1:length(trainID)
        filename = dir2train(trainID(j)).name;
        load(strcat(path2train,filename));
        if strcmp(typeGT,'plurality\frameLevel\')
            trainX = [trainX;monoGT_plurality_frameLevel_X_seg(:,1:end-1)];
            trainY = [trainY;monoGT_plurality_frameLevel_X_seg(:,end)];
        elseif strcmp(typeGT,'plurality\eventLevel\')
            trainX = [trainX;monoGT_plurality_eventLevel_X_seg(:,1:end-1)];
            trainY = [trainY;monoGT_plurality_eventLevel_X_seg(:,end)];
        elseif strcmp(typeGT,'allEvent\frameLevel\')
            trainX = [trainX;monoGT_allEvent_frameLevel_X_seg(:,1:81)];
            trainY = [trainY;monoGT_allEvent_frameLevel_X_seg(:,end)];
        elseif strcmp(typeGT,'allEvent\eventLevel\')
            trainX = [trainX;monoGT_allEvent_eventLevel_X_seg(:,1:end-1)];
            trainY = [trainY;monoGT_allEvent_eventLevel_X_seg(:,end)];
        end
    end
    trainX(trainY==0,:)=[];
    trainY(trainY==0)=[];
    
    % training
    tic
    display('training begins...');
    nTrees = 500;
    rfModel = TreeBagger(nTrees, trainX, trainY, 'Method', 'classification');
    display('training done!');
    toc
    
    % 2_2) TEST
    
    for j = 1:length(testID)
        % load each design X per segment
        filename = dir2test(testID(j)).name; % assumes the same file composition
        load(strcat(path2test,filename));
        if strcmp(typeGT,'plurality\frameLevel\') || strcmp(typeGT,'plurality\eventLevel\')
            testX = monoGT_plurality_frameLevel_X_seg(:,1:end-1);
            testY = monoGT_plurality_frameLevel_X_seg(:,end);
        elseif strcmp(typeGT,'allEvent\frameLevel\') || strcmp(typeGT,'allEvent\eventLevel\')
            testX = monoGT_allEvent_frameLevel_X_seg(:,1:81);
            testY = monoGT_allEvent_frameLevel_X_seg(:,end);
        end
        
        % load the corresponding binary detection
        load(strcat(path2binary,dir2binary(testID(j)).name));
        
        % get seg ID
        [~,filenameOnly,~] = fileparts(filename);
        splitname = strsplit(filenameOnly,'_');
        segID = char(splitname(5));
        
        % detection
        classification = str2double(rfModel.predict(movmean(testX,5)))'; % input moving average
        detectionPerSeg = classification.*detectionPerSeg; % multipy with binary detection
        detectionPerSeg = medfilt1(detectionPerSeg,3); % median filter (10ms)
        display(strcat('detection done for seg',segID));
        
        % save
        detectionPerSeg = roll2index_mono(detectionPerSeg);
        detectionPerSeg(detectionPerSeg(:,1)==0,:) = [];
        filename = strcat(path2save_detect,'detectionPerSeg.',segID,'.mat');
        save(filename,'detectionPerSeg');
                
        % visualization
        rollDetection = index2roll_poly(detectionPerSeg,max_fr,length(class));
        eventGT = roll2index_mono(testY);
        eventGT(eventGT(:,1)==0,:) = [];
        rollGT = index2roll_poly(eventGT,max_fr,16);
       
        %visualize polyform    
        h = figure(1);
        subplot(2,1,1); % GT
        imagesc(time,1,rollGT);
        colormap(flipud(gray));
        set(gca,'YTick',1:length(class),'YTickLabel',class,'TickLength',[0 0]);
        title(strcat('seg.',segID,': GT'));
        
        subplot(2,1,2); % detection
        imagesc(time,1,rollDetection);
        colormap(flipud(gray));
        set(gca,'YTick',1:length(class),'YTickLabel',class,'TickLength',[0 0]);
        title(strcat('seg.',segID,': detection'));        
        
        figurename = strcat(path2save_fig,'detectionPerSeg.',segID,'.fig');
        %set(h,'Position',[300 100 500 300]);
        saveas(gcf,figurename);
        
        % evaluate
        evaluationPerSeg = eventDetectionMetrics_frameBased(rollDetection,rollGT);
        filename = strcat(path2save_eval,'evaluationPerSeg.',segID,'.mat');
        save(filename,'evaluationPerSeg');
        
        close all;
    end
end
