% evaluation poly classification (training with event instances)

% Jaeseong You
% 2016/09/03

% close and clear
clear;
close all;
clc;

% type set-up
type = 1;

% load data
if type == 1 % minimal agreement
    load('D:\AED_repository\gt_archive\poly\agreement\eventLevel_X\polyGT_agreement_eventLevel_X.mat');
    X = polyGT_agreement_eventLevel_X(:,1:end-1);
    Y = polyGT_agreement_eventLevel_X(:,end);
elseif type == 2 % all-event
    load('D:\AED_repository\gt_archive\poly\all_event\eventLevel_X\polyGT_allEvent_eventLevel_X_composite.mat');
    X = polyGT_allEvent_X_composite(:,1:end-1);
    Y = polyGT_allEvent_X_composite(:,end);
end

% parameter setting
numClass = 16; % the minimal agreement does not generate all the 16 classes...
numSample = length(Y); display(numSample);
kSize = round(numSample/5);
k = 5;

% random seed
rng('default');
rng(1);
randSeq = randperm(numSample);

% k-folding
cMatrix = zeros(numClass);
for i = 1:k
    display(i);
    
    % get random index
    randIdx = randSeq;
    
    % train/test group slection 
    if k == 5
        X_test = X(randIdx(kSize*(k-1)+1:end),:);
        Y_test = Y(randIdx(kSize*(k-1)+1:end),:);
        randIdx(kSize*(k-1)+1:end) = [];
        X_train = X(randIdx,:);
        Y_train = Y(randIdx,:);
    else
        X_test = X(randIdx(kSize*(k-1)+1:kSize*k),:);
        Y_test = Y(randIdx(kSize*(k-1)+1:kSize*k),:);
        randIdx(kSize*(k-1)+1:end) = [];
        X_train = exp(X(randIdx(kSize*(k-1)+1:kSize*k),:));
        Y_train = exp(Y(randIdx(kSize*(k-1)+1:kSize*k),:));
    end

    % training
    nTrees = 2000;
    message = 'training begins...'; display(message);
    rfModel = TreeBagger(nTrees, X_train, Y_train, 'Method', 'classification');
    message = 'training done'; display(message);
    
    % prediction
    predict = str2double(rfModel.predict(X_test));
    
    % evaluate
    cMatrix = cMatrix + confusionmat(Y_test,predict,'order',1:16);
    
end

acc = trace(cMatrix)/sum(sum(cMatrix));

%{
acc = 0.4833 (type 1) * lacking...
acc = 0.4548 (type 2)
%}