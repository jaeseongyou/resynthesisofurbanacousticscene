% Jaeseong You
% 2016/10/10
% New York University

% AED ver. 2
% Audio feature computation
%   1) more than 5 annotators
%   2) preprocessing of A-weighting

% clear and close
clear; close all; clc;

%   0) add functions to path
addpath('..\_functions\roll_index');
addpath('..\_functions\external');
addpath(genpath('..\_functions\MIRtoolbox1.6.1'));

display('step 0 completed');


%   1) set up the read/save path and directory

path2save = 'D:\AED_dev\acousticScene_feature\';
path2save_fig = 'D:\AED_dev\acousticScene_feature\figure\';
path2audio = 'D:\AED_dev\acousticScene_raw\';
dir2audio = dir(path2audio);
path2GT = 'D:\AED_dev\gt_archive\mono\binaryDetection\majority\gt\'; % majority GT
dir2GT = dir(path2GT);

display('step 1 completed');


%   2) get the raw annotation data

%       2-0) read in raw annotation data & rearrange
% read in
fid = fopen('D:\AED_dev\rawData_archive\events_rendered.csv');
raw = textscan(fid,'%s','delimiter',',');
fclose(fid);

% rearrange
arranged = reshape(raw{1,1},13,[])';

% remove 'scene's
arranged(strcmp(arranged(:,13),'scene'),:)=[];

%       2-1) figure out the # of annotators per segment
% get the table of 'segment ID : number of annotation'
segID_annID = sortrows(str2double(arranged(:,3:4)),1);
annID = segID_annID(:,2);

% get the points where segment ID changes
segmentTurn = find(diff(segID_annID(:,1))>=1);
uniqueSegID = unique(segID_annID(:,1));

% get # of annotators per segment
segID_annNum = [uniqueSegID zeros(length(uniqueSegID),1)];
for i = 1:length(uniqueSegID)
    begIdx = [1;(segmentTurn+1)]; endIdx = [segmentTurn;length(annID)];
    segID_annNum(i,2) = length(unique(annID(begIdx(i):endIdx(i))));
end

% pick only the ones that are annotated by 5 or more
segID_annNum(segID_annNum(:,2)<=4,:) = [];
segID = segID_annNum(:,1);

display('step 2 completed');


%   3) compute and save features

winsize = 4096;
for i = 1:length(segID)
    
    display(segID(i));
    
    % 3_1) choose the right audio file
    for j = 3:length(dir2audio);
        % find segement ID match and load audio
        filename = dir2audio(j).name;
        splitname = strsplit(filename,'.');
        if cellfun(@str2num,splitname(3)) == segID(i)
            [x,fs] = audioread(strcat(path2audio,filename));
            if size(x,2) == 2
                x = mean(x,2);
            end
            x = x-mean(x);
            display('audio found and loaded');
        end
    end
    
    % plot audio figure and save
    time = (1:length(x))/fs;
    hFig1 = figure(1);
    plot(time,x); ylim([-0.75 0.75]);
    
    filename = strcat('segment.',num2str(segID(i)),'.fig');
    saveas(gcf,strcat(path2save_fig,filename)); 
    
    close all;
    
    % preprocessing
    x = filterA(x,fs); x = x'; % bandpass filtering
    %x = x/max(abs(x)); % normalizing (skip due to rms value..?)
    x(x>1) = 1; x(x<-1) = -1;
    
    
    % 3_2) compute the features
    a = miraudio(x,fs);
    f = mirframe(a,winsize,'sp',winsize/2,'sp');
    s = mirspectrum(f,'Window','hamming');
    
    % getting mir objects
    m_rms           = mirrms(f);
    m_zcr           = mirzerocross(f);
    m_brightness    = mirbrightness(s);
    m_rolloff       = mirrolloff(s);
    m_centroid      = mircentroid(s);
    m_spread        = mirspread(s);
    m_skewness      = mirskewness(s);
    m_kurtosis      = mirkurtosis(s);
    m_flatness      = mirflatness(s);
    m_entropy       = mirentropy(s);
    m_roughness     = mirroughness(s);
    m_irregularity  = mirregularity(s);
    m_inharmonicity = mirinharmonicity(s);
    m_flux          = mirflux(s);
    m_mfcc          = mirmfcc(s);
    
    % getting the data from objects
    f_rms           = mirgetdata(m_rms);
    f_zcr           = mirgetdata(m_zcr);
    f_brightness    = mirgetdata(m_brightness);
    f_rolloff       = mirgetdata(m_rolloff);
    f_centroid      = mirgetdata(m_centroid);
    f_spread        = mirgetdata(m_spread);
    f_skewness      = mirgetdata(m_skewness);
    f_kurtosis      = mirgetdata(m_kurtosis);
    f_flatness      = mirgetdata(m_flatness);
    f_entropy       = mirgetdata(m_entropy);
    f_roughness     = mirgetdata(m_roughness);
    f_irregularity  = mirgetdata(m_irregularity);
    f_inharmonicity = mirgetdata(m_inharmonicity);
    f_flux          = [mirgetdata(m_flux) mean(mirgetdata(m_flux))];
    f_mfcc          = mirgetdata(m_mfcc); 
    
    % remove NaNs
    nanIndex = find(isnan(f_rms)); % 1) RMS
    nanNum = length(nanIndex);
    if nanNum > 0
        for j = 1:nanNum
            f_rms(nanIndex(j)) = nanmean(f_rms);
        end
    end
    nanIndex = find(isnan(f_zcr)); % 2) ZCR
    nanNum = length(nanIndex);
    if nanNum > 0
        for j = 1:nanNum
            f_zcr(nanIndex(j)) = nanmean(f_zcr);
        end
    end
    nanIndex = find(isnan(f_brightness)); % 3) brightness
    nanNum = length(nanIndex);
    if nanNum > 0
        for j = 1:nanNum
            f_brightness(nanIndex(j)) = nanmean(f_brightness);
        end
    end
    nanIndex = find(isnan(f_rolloff)); % 4) roll off
    nanNum = length(nanIndex);
    if nanNum > 0
        for j = 1:nanNum
            f_rolloff(nanIndex(j)) = nanmean(f_rolloff);
        end
    end
    nanIndex = find(isnan(f_centroid)); % 5) centroid
    nanNum = length(nanIndex);
    if nanNum > 0
        for j = 1:nanNum
            f_centroid(nanIndex(j)) = nanmean(f_centroid);
        end
    end
    nanIndex = find(isnan(f_spread)); % 6) spread
    nanNum = length(nanIndex);
    if nanNum > 0
        for j = 1:nanNum
            f_spread(nanIndex(j)) = nanmean(f_spread);
        end
    end
    nanIndex = find(isnan(f_skewness)); % 7) skewness
    nanNum = length(nanIndex);
    if nanNum > 0
        for j = 1:nanNum
            f_skewness(nanIndex(j)) = nanmean(f_skewness);
        end
    end
    nanIndex = find(isnan(f_kurtosis)); % 8) kurtosis
    nanNum = length(nanIndex);
    if nanNum > 0
        for j = 1:nanNum
            f_kurtosis(nanIndex(j)) = nanmean(f_kurtosis);
        end
    end
    nanIndex = find(isnan(f_flatness)); % 9) flatness
    nanNum = length(nanIndex);
    if nanNum > 0
        for j = 1:nanNum
            f_flatness(nanIndex(j)) = nanmean(f_flatness);
        end
    end
    nanIndex = find(isnan(f_entropy)); % 10) entropy
    nanNum = length(nanIndex);
    if nanNum > 0
        for j = 1:nanNum
            f_entropy(nanIndex(j)) = nanmean(f_entropy);
        end
    end
    nanIndex = find(isnan(f_roughness)); % 11) roughness
    nanNum = length(nanIndex);
    if nanNum > 0
        for j = 1:nanNum
            f_roughness(nanIndex(j)) = nanmean(f_roughness);
        end
    end
    nanIndex = find(isnan(f_irregularity)); % 12) irregularity
    nanNum = length(nanIndex);
    if nanNum > 0
        for j = 1:nanNum
            f_irregularity(nanIndex(j)) = nanmean(f_irregularity);
        end
    end
    nanIndex = find(isnan(f_inharmonicity)); % 13) inharmonicity
    nanNum = length(nanIndex);
    if nanNum > 0
        for j = 1:nanNum
            f_inharmonicity(nanIndex(j)) = nanmean(f_inharmonicity);
        end
    end
    nanIndex = find(isnan(f_flux)); % 14) flux
    nanNum = length(nanIndex);
    if nanNum > 0
        for j = 1:nanNum
            f_flux(nanIndex(j)) = nanmean(f_flux);
        end
    end
    [nanRow,nanCol] = find(isnan(f_mfcc)); % 15) mfcc
    nanNum = length(nanCol);
    if nanNum > 0
        for j = 1:nanNum
            f_mfcc(nanRow(j),nanCol(j)) = nanmean(f_mfcc(nanRow(j),:));
        end
    end

    % getting the first derivatives
    d_rms           = [diff(f_rms) mean(diff(f_rms))];
    d_zcr           = [diff(f_zcr) mean(diff(f_zcr))];
    d_brightness    = [diff(f_brightness) mean(diff(f_brightness))];
    d_rolloff       = [diff(f_rolloff) mean(diff(f_rolloff))];
    d_centroid      = [diff(f_centroid) mean(diff(f_centroid))];
    d_spread        = [diff(f_spread) mean(diff(f_spread))];
    d_skewness      = [diff(f_skewness) mean(diff(f_skewness))];
    d_kurtosis      = [diff(f_kurtosis) mean(diff(f_kurtosis))];
    d_flatness      = [diff(f_flatness) mean(diff(f_flatness))];
    d_entropy       = [diff(f_entropy) mean(diff(f_entropy))];
    d_roughness     = [diff(f_roughness) mean(diff(f_roughness))];
    d_irregularity  = [diff(f_irregularity) mean(diff(f_irregularity))];
    d_inharmonicity = [diff(f_inharmonicity) mean(diff(f_inharmonicity))];
    d_flux          = [diff(f_flux) mean(diff(f_flux))];
    d_mfcc          = [diff(f_mfcc,1,2) mean(diff(f_mfcc,1,2),2)];
    
    % getting the second derivatives
    dd_rms          = [diff(d_rms) mean(diff(d_rms))];
    dd_zcr          = [diff(d_zcr) mean(diff(d_zcr))];
    dd_brightness   = [diff(d_brightness) mean(diff(d_brightness))];
    dd_rolloff      = [diff(d_rolloff) mean(diff(d_rolloff))];
    dd_centroid     = [diff(d_centroid) mean(diff(d_centroid))];
    dd_spread       = [diff(d_spread) mean(diff(d_spread))];
    dd_skewness     = [diff(d_skewness) mean(diff(d_skewness))];
    dd_kurtosis     = [diff(d_kurtosis) mean(diff(d_kurtosis))];
    dd_flatness     = [diff(d_flatness) mean(diff(d_flatness))];
    dd_entropy      = [diff(d_entropy) mean(diff(d_entropy))];
    dd_roughness    = [diff(d_roughness) mean(diff(d_roughness))];
    dd_irregularity = [diff(d_irregularity) mean(diff(d_irregularity))];
    dd_inharmonicity= [diff(d_inharmonicity) mean(diff(d_inharmonicity))];
    dd_flux         = [diff(d_flux) mean(diff(d_flux))];
    dd_mfcc         = [diff(d_mfcc,1,2) mean(diff(d_mfcc,1,2),2)]; 
    
    % putting features together
    featurePerSeg =   [f_rms;f_zcr;f_brightness;f_rolloff;f_centroid;f_spread;...
                 f_skewness;f_kurtosis;f_flatness;f_entropy;f_roughness;f_irregularity;f_inharmonicity;f_flux;f_mfcc;...
                 d_rms;d_zcr;d_brightness;d_rolloff;d_centroid;d_spread;...
                 d_skewness;d_kurtosis;d_flatness;d_entropy;d_roughness;d_irregularity;d_inharmonicity;d_flux;d_mfcc;...
                 dd_rms;dd_zcr;dd_brightness;dd_rolloff;dd_centroid;dd_spread;...
                 dd_skewness;dd_kurtosis;dd_flatness;dd_entropy;dd_roughness;dd_irregularity;dd_inharmonicity;dd_flux;dd_mfcc]';
    
    % check NaN once more
    numNaN = sum(sum((isnan(featurePerSeg))));
    if numNaN == 0
        display('good to go');
    else
        display('fishy...');
    end
             
    % save feature per segment
    fileName = strcat('featurePerSeg.',num2str(segID(i)));
    save(strcat(path2save,fileName,'.mat'),'featurePerSeg');
    
end

display('step 3 completed');

 