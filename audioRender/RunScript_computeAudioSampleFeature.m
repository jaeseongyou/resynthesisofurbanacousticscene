% 2016/10/10
% Jaeseong You
% New York University

% Does: 
%       1) Compute and save the audio features of collected samples on the frame level 

% Generates: 
%       1) feature.(sampleName).mat

% clear and close
clear; close all; clc;

% add necessary functions to path
addpath('..\_functions\external');
addpath(genpath('..\_functions\MIRtoolbox1.6.1'));

% header preparation
header = {'rms','zcr','bright','rolloff','centroid','spread',...
        'skewness','kurtosis','flatness','entropy','roughness','irregularity','inharmonicity','flux',...
        'mfcc1','mfcc2','mfcc3','mfcc4','mfcc5','mfcc6','mfcc7','mfcc8','mfcc9','mfcc10','mfcc11','mfcc12','mfcc13',...
        'd_rms','d_zcr','d_bright','d_rolloff','d_centroid','d_spread',...
        'd_skewness','d_kurtosis','d_flatness','d_entropy','d_roughness','d_irregularity','d_inharmonicity','d_flux',...
        'd_mfcc1','d_mfcc2','d_mfcc3','d_mfcc4','d_mfcc5','d_mfcc6','d_mfcc7','d_mfcc8','d_mfcc9','d_mfcc10','d_mfcc11','d_mfcc12','d_mfcc13',...
        'dd_rms','dd_zcr','dd_bright','dd_rolloff','dd_centroid','dd_spread',...
        'dd_skewness','dd_kurtosis','dd_flatness','dd_entropy','dd_roughness','dd_irregularity','dd_inharmonicity','dd_flux',...
        'dd_mfcc1','dd_mfcc2','dd_mfcc3','dd_mfcc4','dd_mfcc5','dd_mfcc6','dd_mfcc7','dd_mfcc8','dd_mfcc9','dd_mfcc10','dd_mfcc11','dd_mfcc12','dd_mfcc13'};
numFeature = length(header);

%% 1) Read in the raw pool

pathPool = 'D:\AED_dev\audioSample_raw\';
dirPool = dir(pathPool);
numClass = length(dirPool)-2;

%% 2) Iterate through each class

winsize = 4096;
for i = 3:numClass+2
    
    className = dirPool(i).name;
    dirClassPool = dir(strcat(pathPool,className)); 
    numSample = length(dirClassPool)-2;
    
    path2save = strcat('D:\AED_dev\audioSample_feature\',className);
    % compute the samples
    for j = 3:numSample+2
        
        % read audio
        audioName = dirClassPool(j).name;
        [x,fs] = audioread(strcat(pathPool,className,'\',audioName));
        
        % change to mono
        if size(x,2) == 2
            x = mean(x,2);
        end
        
        % remove DC
        x = x-mean(x);
        
        % getting the audio ready 
        a = miraudio(x,fs);
        f = mirframe(a,winsize,'sp',winsize/2,'sp');
        s = mirspectrum(f,'Window','hamming');

        % getting mir objects
        m_rms           = mirrms(f);
        m_zcr           = mirzerocross(f);
        m_brightness    = mirbrightness(s);
        m_rolloff       = mirrolloff(s);
        m_centroid      = mircentroid(s);
        m_spread        = mirspread(s);
        m_skewness      = mirskewness(s);
        m_kurtosis      = mirkurtosis(s);
        m_flatness      = mirflatness(s);
        m_entropy       = mirentropy(s);
        m_roughness     = mirroughness(s);
        m_irregularity  = mirregularity(s);
        m_inharmonicity = mirinharmonicity(s);
        m_flux          = mirflux(s);
        m_mfcc          = mirmfcc(s);
    
        % getting the data from objects
        f_rms           = mirgetdata(m_rms);
        f_zcr           = mirgetdata(m_zcr);
        f_brightness    = mirgetdata(m_brightness);
        f_rolloff       = mirgetdata(m_rolloff);
        f_centroid      = mirgetdata(m_centroid);
        f_spread        = mirgetdata(m_spread);
        f_skewness      = mirgetdata(m_skewness);
        f_kurtosis      = mirgetdata(m_kurtosis);
        f_flatness      = mirgetdata(m_flatness);
        f_entropy       = mirgetdata(m_entropy);
        f_roughness     = mirgetdata(m_roughness);
        f_irregularity  = mirgetdata(m_irregularity);
        f_inharmonicity = mirgetdata(m_inharmonicity);
        f_flux          = [mirgetdata(m_flux) mean(mirgetdata(m_flux))];
        f_mfcc          = mirgetdata(m_mfcc); 
   
        % before getting the first and the second derivatives, remove NaNs
        nanIndex = find(isnan(f_rms)); % 1) RMS
        nanNum = length(nanIndex);
        if nanNum > 0
            for k = 1:nanNum
                f_rms(nanIndex(k)) = nanmean(f_rms);
            end
        end
        nanIndex = find(isnan(f_zcr)); % 2) ZCR
        nanNum = length(nanIndex);
        if nanNum > 0
            for k = 1:nanNum
                f_zcr(nanIndex(k)) = nanmean(f_zcr);
            end
        end
        nanIndex = find(isnan(f_brightness)); % 3) brightness
        nanNum = length(nanIndex);
        if nanNum > 0
            for k = 1:nanNum
                f_brightness(nanIndex(k)) = nanmean(f_brightness);
            end
        end
        nanIndex = find(isnan(f_rolloff)); % 4) roll off
        nanNum = length(nanIndex);
        if nanNum > 0
            for k = 1:nanNum
                f_rolloff(nanIndex(k)) = nanmean(f_rolloff);
            end
        end
        nanIndex = find(isnan(f_centroid)); % 5) centroid
        nanNum = length(nanIndex);
        if nanNum > 0
            for k = 1:nanNum
                f_centroid(nanIndex(k)) = nanmean(f_centroid);
            end
        end
        nanIndex = find(isnan(f_spread)); % 6) spread
        nanNum = length(nanIndex);
        if nanNum > 0
            for k = 1:nanNum
                f_spread(nanIndex(k)) = nanmean(f_spread);
            end
        end
        nanIndex = find(isnan(f_skewness)); % 7) skewness
        nanNum = length(nanIndex);
        if nanNum > 0
            for k = 1:nanNum
                f_skewness(nanIndex(k)) = nanmean(f_skewness);
            end
        end
        nanIndex = find(isnan(f_kurtosis)); % 8) kurtosis
        nanNum = length(nanIndex);
        if nanNum > 0
            for k = 1:nanNum
                f_kurtosis(nanIndex(k)) = nanmean(f_kurtosis);
            end
        end
        nanIndex = find(isnan(f_flatness)); % 9) flatness
        nanNum = length(nanIndex);
        if nanNum > 0
            for k = 1:nanNum
                f_flatness(nanIndex(k)) = nanmean(f_flatness);
            end
        end
        nanIndex = find(isnan(f_entropy)); % 10) entropy
        nanNum = length(nanIndex);
        if nanNum > 0
            for k = 1:nanNum
                f_entropy(nanIndex(k)) = nanmean(f_entropy);
            end
        end
        nanIndex = find(isnan(f_roughness)); % 11) roughness
        nanNum = length(nanIndex);
        if nanNum > 0
            for k = 1:nanNum
                f_roughness(nanIndex(k)) = nanmean(f_roughness);
            end
        end
        nanIndex = find(isnan(f_irregularity)); % 12) irregularity
        nanNum = length(nanIndex);
        if nanNum > 0
            for k = 1:nanNum
                f_irregularity(nanIndex(k)) = nanmean(f_irregularity);
            end
        end
        nanIndex = find(isnan(f_inharmonicity)); % 13) inharmonicity
        nanNum = length(nanIndex);
        if nanNum > 0
            for k = 1:nanNum
                f_inharmonicity(nanIndex(k)) = nanmean(f_inharmonicity);
            end
        end
        nanIndex = find(isnan(f_flux)); % 14) flux
        nanNum = length(nanIndex);
        if nanNum > 0
            for k = 1:nanNum
                f_flux(nanIndex(k)) = nanmean(f_flux);
            end
        end

        [nanRow,nanCol] = find(isnan(f_mfcc)); % 15) flux
        nanNum = length(nanCol);
        if nanNum > 0
            for k = 1:nanNum
                f_mfcc(nanRow(j),nanCol(k)) = nanmean(f_mfcc(nanRow(k),:));
            end
        end

        % getting the first derivatives
        d_rms           = [diff(f_rms) mean(diff(f_rms))];
        d_zcr           = [diff(f_zcr) mean(diff(f_zcr))];
        d_brightness    = [diff(f_brightness) mean(diff(f_brightness))];
        d_rolloff       = [diff(f_rolloff) mean(diff(f_rolloff))];
        d_centroid      = [diff(f_centroid) mean(diff(f_centroid))];
        d_spread        = [diff(f_spread) mean(diff(f_spread))];
        d_skewness      = [diff(f_skewness) mean(diff(f_skewness))];
        d_kurtosis      = [diff(f_kurtosis) mean(diff(f_kurtosis))];
        d_flatness      = [diff(f_flatness) mean(diff(f_flatness))];
        d_entropy       = [diff(f_entropy) mean(diff(f_entropy))];
        d_roughness     = [diff(f_roughness) mean(diff(f_roughness))];
        d_irregularity  = [diff(f_irregularity) mean(diff(f_irregularity))];
        d_inharmonicity = [diff(f_inharmonicity) mean(diff(f_inharmonicity))];
        d_flux          = [diff(f_flux) mean(diff(f_flux))];
        d_mfcc          = [diff(f_mfcc,1,2) mean(diff(f_mfcc,1,2),2)];

        % getting the second derivatives
        dd_rms          = [diff(d_rms) mean(diff(d_rms))];
        dd_zcr          = [diff(d_zcr) mean(diff(d_zcr))];
        dd_brightness   = [diff(d_brightness) mean(diff(d_brightness))];
        dd_rolloff      = [diff(d_rolloff) mean(diff(d_rolloff))];
        dd_centroid     = [diff(d_centroid) mean(diff(d_centroid))];
        dd_spread       = [diff(d_spread) mean(diff(d_spread))];
        dd_skewness     = [diff(d_skewness) mean(diff(d_skewness))];
        dd_kurtosis     = [diff(d_kurtosis) mean(diff(d_kurtosis))];
        dd_flatness     = [diff(d_flatness) mean(diff(d_flatness))];
        dd_entropy      = [diff(d_entropy) mean(diff(d_entropy))];
        dd_roughness    = [diff(d_roughness) mean(diff(d_roughness))];
        dd_irregularity = [diff(d_irregularity) mean(diff(d_irregularity))];
        dd_inharmonicity= [diff(d_inharmonicity) mean(diff(d_inharmonicity))];
        dd_flux         = [diff(d_flux) mean(diff(d_flux))];
        dd_mfcc         = [diff(d_mfcc,1,2) mean(diff(d_mfcc,1,2),2)];

        % putting features together
        sampleFeature =   [f_rms;f_zcr;f_brightness;f_rolloff;f_centroid;f_spread;...
                     f_skewness;f_kurtosis;f_flatness;f_entropy;f_roughness;f_irregularity;f_inharmonicity;f_flux;f_mfcc;...
                     d_rms;d_zcr;d_brightness;d_rolloff;d_centroid;d_spread;...
                     d_skewness;d_kurtosis;d_flatness;d_entropy;d_roughness;d_irregularity;d_inharmonicity;d_flux;d_mfcc;...
                     dd_rms;dd_zcr;dd_brightness;dd_rolloff;dd_centroid;dd_spread;...
                     dd_skewness;dd_kurtosis;dd_flatness;dd_entropy;dd_roughness;dd_irregularity;dd_inharmonicity;dd_flux;dd_mfcc]';

        % check NaN once more
        numNaN = sum(sum((isnan(sampleFeature))));
        if numNaN == 0
            display('good to go');
        else
            display('fishy...');
        end
        
        % put in output
        featurePerSample = mean(sampleFeature);
        
        % save
        [~,filename,~] = fileparts(audioName);
        save(strcat(path2save,'\',filename,'.mat'),'featurePerSample');

    end
    
end

